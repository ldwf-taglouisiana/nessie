# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130220195605) do

  create_table "activity_log_types", :force => true do |t|
    t.string   "activity_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "activity_logs", :force => true do |t|
    t.integer  "activity_type"
    t.string   "item_id"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "anglers", :id => false, :force => true do |t|
    t.integer  "id"
    t.integer  "lag_no"
    t.integer  "law_no"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "street"
    t.string   "suite"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone_number_1"
    t.string   "phone_number_1_type"
    t.string   "phone_number_2"
    t.string   "phone_number_2_type"
    t.string   "phone_number_3"
    t.string   "phone_number_3_type"
    t.string   "phone_number_4"
    t.string   "phone_number_4_type"
    t.string   "email"
    t.integer  "tag_end_user_type"
    t.integer  "shirt_size"
    t.boolean  "deleted"
    t.integer  "lax_no"
    t.string   "email_2"
    t.string   "user_name"
    t.text     "comments"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "angler_id",           :limit => 20, :null => false
  end

  add_index "anglers", ["angler_id", "first_name", "last_name"], :name => "index_anglers_on_angler_id_and_first_name_and_last_name"

  create_table "bait_type_options", :force => true do |t|
    t.string   "bait_type_option"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "captures", :force => true do |t|
    t.string   "angler_id",             :limit => 20
    t.integer  "captain_id"
    t.integer  "tag_id"
    t.datetime "capture_date"
    t.integer  "species_id"
    t.float    "length"
    t.float    "estimated_length"
    t.text     "location_description"
    t.float    "lat"
    t.float    "long"
    t.integer  "fish_condition_id"
    t.integer  "fish_tagged_in_id"
    t.integer  "landing_net_used_id"
    t.integer  "hook_type_id"
    t.integer  "bait_type_id"
    t.integer  "hook_removed_id"
    t.integer  "hook_barbed_id"
    t.float    "weight"
    t.float    "estimated_weight"
    t.float    "water_temperature"
    t.float    "water_salinity"
    t.float    "wind_speed"
    t.integer  "wind_direction_id"
    t.text     "comments"
    t.boolean  "confirmed"
    t.boolean  "verified"
    t.datetime "entry_date"
    t.boolean  "deleted"
    t.boolean  "mailed_map"
    t.boolean  "recapture"
    t.integer  "recapture_disposition"
    t.boolean  "see_card"
    t.integer  "time_of_day"
    t.datetime "created_at",                                                     :null => false
    t.datetime "updated_at",                                                     :null => false
    t.spatial  "geom",                  :limit => {:srid=>0, :type=>"geometry"}
    t.integer  "old_angler_id"
    t.string   "entered_gps_type"
  end

  add_index "captures", ["angler_id"], :name => "idx_capture_angler_id"
  add_index "captures", ["capture_date"], :name => "idx_capture_date"
  add_index "captures", ["geom"], :name => "index_captures_geom", :spatial => true

  create_table "fish_condition_options", :force => true do |t|
    t.string   "fish_condition_option"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "fish_tagged_options", :force => true do |t|
    t.string   "fish_tagged_option"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "girth_accuracy_options", :force => true do |t|
    t.string   "girth_accuracy_option"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "hook_barbed_options", :force => true do |t|
    t.string   "hook_barbed_option"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "hook_removed_options", :force => true do |t|
    t.string   "hook_removed_option"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "hook_type_options", :force => true do |t|
    t.string   "hook_type_option"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "landing_net_used_options", :force => true do |t|
    t.string   "landing_net_used_option"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  create_table "length_accuracy_options", :force => true do |t|
    t.string   "length_accuracy_option"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "manufacturers", :force => true do |t|
    t.string   "name"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "zip"
    t.string   "contact"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "marinas", :force => true do |t|
    t.integer  "site_no"
    t.string   "name"
    t.boolean  "closed"
    t.string   "city"
    t.string   "parish"
    t.boolean  "publish"
    t.string   "address"
    t.float    "latitude"
    t.float    "longitude"
    t.text     "web_address"
    t.string   "phone"
    t.string   "hours_of_operation"
    t.boolean  "can_get_fishing_license"
    t.string   "fee_amount"
    t.boolean  "has_bait"
    t.boolean  "has_ice"
    t.boolean  "has_food"
    t.boolean  "has_restrooms"
    t.boolean  "has_sewage_pump"
    t.boolean  "has_cert_scales"
    t.string   "facility_type"
    t.boolean  "has_cleaning_station"
    t.string   "fuel_type"
    t.text     "location_description"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "deleted",                 :default => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  create_table "phone_type_options", :force => true do |t|
    t.string   "phone_type_option"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "photos", :force => true do |t|
    t.string   "content_type"
    t.string   "filename"
    t.binary   "binary_data"
    t.integer  "capture_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "recapture_dispositions", :force => true do |t|
    t.string   "recapture_disposition"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "referrals", :force => true do |t|
    t.integer  "referred_by"
    t.string   "last_name"
    t.string   "first_name"
    t.string   "street"
    t.string   "suite"
    t.string   "city"
    t.integer  "state"
    t.string   "zip"
    t.string   "phone"
    t.integer  "phone_type"
    t.string   "email"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "segments", :primary_key => "gid", :force => true do |t|
    t.decimal "objectid",                                                     :precision => 10, :scale => 0
    t.decimal "area"
    t.float   "perimeter"
    t.float   "subseg02"
    t.float   "subseg02id"
    t.string  "basin",      :limit => 18
    t.string  "subbasin",   :limit => 10
    t.string  "subbasin_n", :limit => 254
    t.string  "sgmnt_name", :limit => 165
    t.string  "segment",    :limit => 10
    t.spatial "the_geom",   :limit => {:srid=>26915, :type=>"multi_polygon"}
  end

  add_index "segments", ["the_geom"], :name => "segments_the_geom_gist", :spatial => true

  create_table "shirt_size_options", :force => true do |t|
    t.string   "shirt_size_option"
    t.integer  "item_ordinal_pos"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "species", :force => true do |t|
    t.string   "common_name"
    t.string   "species_code"
    t.integer  "position"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "proper_name"
    t.string   "other_name"
    t.string   "family"
    t.text     "habitat"
    t.text     "description"
    t.text     "size"
    t.text     "food_value"
  end

  create_table "species_lists", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "states", :force => true do |t|
    t.string   "state_abbr"
    t.string   "state"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tag_end_user_types", :force => true do |t|
    t.string   "name"
    t.boolean  "deleted"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tag_lot_colors", :force => true do |t|
    t.string   "color"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tag_lot_fixes", :force => true do |t|
    t.integer  "tag_lot_id"
    t.string   "tag_no"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tag_lots", :force => true do |t|
    t.integer  "tag_lot_number"
    t.integer  "manufacturer_id"
    t.string   "prefix"
    t.integer  "start_no"
    t.integer  "end_no"
    t.integer  "end_user_type"
    t.integer  "cost"
    t.datetime "date_entered"
    t.boolean  "ordered"
    t.datetime "order_date"
    t.boolean  "received"
    t.datetime "received_date"
    t.boolean  "deleted"
    t.integer  "color"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "tag_requests", :force => true do |t|
    t.integer  "angler_id"
    t.integer  "num_tags_requested"
    t.datetime "date_requested"
    t.boolean  "fulfilled"
    t.datetime "date_fulfilled"
    t.boolean  "deleted"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "tags", :force => true do |t|
    t.string   "angler_id",       :limit => 20
    t.string   "tag_no"
    t.integer  "tag_lot_id"
    t.integer  "tag_request_id"
    t.boolean  "deleted"
    t.integer  "unassign_option"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "old_anlger_id"
  end

  add_index "tags", ["tag_no"], :name => "idx_tags_tag_no"

  create_table "time_of_day_options", :force => true do |t|
    t.string   "time_of_day_option"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "transaction_logs", :force => true do |t|
    t.datetime "date_time"
    t.string   "username"
    t.string   "action"
    t.string   "data_set"
    t.integer  "record_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "unassign_tag_options", :force => true do |t|
    t.string   "unassign_tag_option"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "user_tag_requests", :force => true do |t|
    t.string   "angler_id"
    t.integer  "number_of_tags"
    t.boolean  "fulfilled"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "tag_type"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "angler_id"
    t.string   "authentication_token"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
