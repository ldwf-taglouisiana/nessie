module SignInModule

  def module_sign_in(hash)
    site_url = API_SERVER['base_url']
    oauth_client = OAuth2::Client.new(ENV['APP_ID'], ENV['APP_SECRET'], {site: site_url})
    begin
      access_token = oauth_client.password.get_token("#{hash[:email]}", "#{hash[:password]}", :headers => {'Fiery-Mango' => 'yumm'})
    rescue
      flash[:error] = "Invalid username or password."
      redirect_to new_user_session_path
      return false
    end
    raw_info ||= access_token.get('/api/v1/me.json').parsed

    auth_hash = Hash.new
    auth_hash[:provider] = 'taglouisiana'
    auth_hash[:uid] = raw_info['user']['id'].to_s
    auth_hash[:info] = OpenStruct.new({:email => raw_info['user']['email'],
                                       :first_name => raw_info['user']['first_name'],
                                       :last_name => raw_info['user']['last_name'],
                                       :name => "#{raw_info['user']['first_name']} #{raw_info['user']['last_name']}"})

    auth_hash[:credentials] = OpenStruct.new({:token => access_token.token})
    auth = OpenStruct.new(auth_hash)
    @user = User.find_for_taglouisiana_oauth(auth, current_user)

    if @user.persisted?
      sign_in @user, :event => :authentication #this will throw if @user is not activated
      respond_to do |format|
        format.js { render :js => "window.location = '/dashboard'" }
        format.html { redirect_to dashboard_path }
      end
      #set_flash_message(:notice, :success, :kind => "Tag Louisiana") if is_navigational_format?
    else
      session["devise.taglouisiana_data"] = auth_hash
      redirect_to new_user_registration_url
    end
  end

end