#-*- mode: ruby -*-

#vi: set ft=ruby :

=begin
There is a lot going on in this file.

Let me break it down.....

We are using multi VM so that the nessie box can communicate with the Narwhal server.
This mimics a similar multi box setup. Which gives us a better dev environment.

As of Vagrant 1.8.x we can use the new linked_clone command which gives us differential disks,
reducing host hard disk space.

By default the Narwhal boots a new VM in the 'dev' branch using a fresh copy of the remote database.
We can skip some of these options with ENV variables.

Running the fallowing will change the branch Narwhal:
NARWHAL_BRANCH=master vagrant up

We can tell the VM to use the host's current Narwhal dev directory, instead of cloning from git (only effects provisioning):
NARWHAL_PATH=/path/to/dev/narwhal vagrant up

Setting the NARWHAL_PATH will enable NFS on that directory so any changed on the host will cascade to the
the VM here.

We can also skip the remote DB download with the following (only effects provisioning):
NARWHAL_SKIP_DB_DOWNLOAD=true vagrant up

You can combine any of the options that you wish.

By default both servers start automatically on vagrant up
=end


NARWHAL_BRANCH = ENV['NARWHAL_BRANCH'] || 'dev'
NARWHAL_PATH = ENV['NARWHAL_PATH']
NARWHAL_SKIP_DB_DOWNLOAD = ENV['NARWHAL_SKIP_DB_DOWNLOAD'] || 'false'

narwhal = {
    primary: false,
    name: "narwhal",
    box: "tridentuno/generic",
    eth1: "192.168.205.11",
    web_port: 4000,
    mem: 512,
    cpu: 1,
    config_scripts: [
        'config/vagrant/scripts/narwhal_config_nopath.sh',
        'config/vagrant/scripts/narwhal_config_with_path.sh'
    ],
    startup_scripts: [
        # left blank
    ]
}
nessie = {
    primary: true,
    name: "nessie",
    box: "tridentuno/generic",
    eth1: "192.168.205.10",
    web_port: 3000,
    mem: 512,
    cpu: 1,
    config_scripts: [
        'config/vagrant/scripts/nessie_config.sh'
    ],
    startup_scripts: [
        # left blank
    ]
}

Vagrant.configure(2) do |config|

  config.vm.define :narwhal, primary: false do |box_config|
    # narwhal = boxes[:narwhal]

    box_config.vm.box = narwhal[:box]

    if NARWHAL_PATH
      box_config.vm.synced_folder NARWHAL_PATH, "/vagrant", type: 'nfs'
    else
      box_config.vm.synced_folder "/dev/null", "/vagrant", type: 'nfs', disabled: true
    end

    box_config.vm.network :forwarded_port, guest: 3000, host: narwhal[:web_port]

    box_config.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--memory", narwhal[:mem]]
      v.customize ["modifyvm", :id, "--cpus", narwhal[:cpu]]

      v.linked_clone = true if Vagrant::VERSION =~ /^1.8/
    end

    config.vm.provision "shell", privileged: false, run: 'once' do |s|
      s.path = 'config/vagrant/scripts/narwhal_general_config.sh'
    end

    if NARWHAL_PATH
      config.vm.provision "shell", privileged: false, run: 'once' do |s|
        s.path = narwhal[:config_scripts][1]
        s.args = "\"#{NARWHAL_BRANCH}\" \"#{NARWHAL_SKIP_DB_DOWNLOAD}\""
      end
    else
      config.vm.provision "shell", privileged: false, run: 'once' do |s|
        s.path = narwhal[:config_scripts][0]
        s.args = "\"#{NARWHAL_BRANCH}\" \"#{NARWHAL_SKIP_DB_DOWNLOAD}\""
      end
    end

    config.vm.provision "shell", privileged: false, run: 'always' do |s|
      s.path = 'config/vagrant/scripts/narwhal_startup.sh'
      s.args = "\"#{NARWHAL_BRANCH}\""
    end

    box_config.vm.network :private_network, ip: narwhal[:eth1]
  end

  config.vm.define :nessie, primary: true do |box_config|
    # nessie = boxes[:nessie]

    box_config.vm.box = nessie[:box]

    box_config.vm.synced_folder ".", "/vagrant", type: 'nfs'

    box_config.vm.network :forwarded_port, guest: 5432, host: 5432
    box_config.vm.network :forwarded_port, guest: 3000, host: nessie[:web_port]


    box_config.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--memory", nessie[:mem]]
      v.customize ["modifyvm", :id, "--cpus", nessie[:cpu]]

      v.linked_clone = true if Vagrant::VERSION =~ /^1.8/
    end

    nessie[:config_scripts].each do |script|
      config.vm.provision "shell", privileged: false, run: 'once' do |s|
        s.path = script
      end
    end

    config.vm.provision "shell", privileged: false, run: 'always' do |s|
      s.path = 'config/vagrant/scripts/nessie_startup.sh'
    end

    box_config.vm.network :private_network, ip: nessie[:eth1]
  end
end
