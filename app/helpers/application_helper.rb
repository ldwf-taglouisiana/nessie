module ApplicationHelper

  # Helper method for sorting captures by column on the "My Captures" and "My Draft Captures" pages
  def sortable(column, title = nil, start_date = nil, end_date = nil, species = nil)
    title ||= column.titleize
    direction = (column == params[:sort] && params[:direction] == "asc") ? "desc" : "asc"
    link_to title, :sort => column, :direction => direction, :start_date => start_date, :end_date => end_date, :species => species
  end

  def bootstrap_class_for flash_type
    case flash_type
      when :success
        "alert alert-success"
      when :error
        "alert alert-danger"
      when :alert
        "alert alert-block"
      when :notice
        "alert alert-info"
      else
        flash_type.to_s
    end
  end

  def has_pending_captures?
    return false if @access_token.nil?

    objects = @access_token.get("#{API_SERVER['address']}draft_captures.json").parsed
    current_user and objects["items"].count > 0
  end
end
