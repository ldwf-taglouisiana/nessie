class ContactUs < ActionMailer::Base
  default from: "Tag Louisiana <no-reply@taglouisiana.com>"

  def site_feedback(params)
    @name = params[:name]
    @email = params[:email]
    @feedback = params[:feedback]
    mail(to: 'hdavid@wlf.la.gov; mcrouch@wlf.la.gov', subject: 'Site Feedback from ' + @name)
  end
end
