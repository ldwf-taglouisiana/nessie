class PasswordResetRequest < ActionMailer::Base
  default from: "Tag Louisiana <no-repley@taglouisiana.com>"

  def password_reset_request(user,token)
    @user = user
    @token = token
    mail(to: @user.email, subject: 'Password Reset Request')
  end
end
