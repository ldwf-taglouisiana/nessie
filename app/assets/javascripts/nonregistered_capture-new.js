$(document).ready(function(){

  // force a click for the recapture label.
  $('.onoffswitch-label').trigger('click');

  // set the recapture field to true and disable input from it
  $('.recaptureField').val(true);
  $('.recaptureValue').attr('disabled', 'disabled');
  $('.onoffswitch-checkbox').attr('disabled', 'disabled');

  // set the focus to the firs tname field
  $('.firstNameField').focus();

  // remove the multiple capture naming
  $('[name]').each(function(){
    var name = $(this).attr('name').replace('[0]', '');
    $(this).attr('name', name);
  });
});
