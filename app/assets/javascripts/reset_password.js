$(document).ready(function(){
    // enable the validations on the form
    attachValidation();
});

function attachValidation() {
    var form = $('#passwordResetForm');

    // will refresh the buttons so they are usable again.
    var refreshButtons = function() {
        $('.submit').removeAttr('disabled');
    };

    var failure = function () {
        console.log('fail');
        refreshButtons();
    };

    form.validationEngine('attach',{
        promptPosition : "centerRight",
        prettySelect: true,
        focusFirstField: false,
        onFailure: failure
    });
}