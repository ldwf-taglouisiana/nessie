$(document).ready(function(){
    $('#start_time').change(function() {
        $('#real_start_time').val($(this).find(':selected').text());
        displayTime();
    });

    $('#end_time').change(function() {
        $('#real_end_time').val($(this).find(':selected').text());
        displayTime();
    });
    $('.datepicker').datepicker();

    // Attach the validations to the form
    $("#vol_submit" ).validationEngine('attach');

});

function displayTime(){
    var start = $('#start_time').find(':selected').val();//$('#start_time option:selected').text().split(" ")[0];
    var end = $('#end_time').find(':selected').val();//$('#end_time option:selected').text().split(" ")[0];
    var total = TimeDiff(end,start);
    var time = total.split(":");
    var outPutString = time[0] + " hour(s) " + time[1] + " minute(s)";
    if(time[0] != "NaN" && time[1] != "NaN"){
        $('.detail i').text(outPutString);
    }
}

  function TimeDiff(a,b)
  {
     var ret = 0;
     var value = (a-b)/2;
      // If the start time is after the end time, move the difference forward by one day
      if (value <=0)
      {
          value = value + 24;
      }
      if (value % 1 != 0)
      {
          ret = Math.floor(value) + ":30";
      }
      else ret = value + ":00";

      return ret;
  }
//function TimeDiff(a,b)
//{
//    var first = a.split(":")
//    var second = b.split(":")
//    var xx;
//    var yy;
//    if(parseInt(first[0]) < parseInt(second[0])){
//
//        if(parseInt(first[1]) < parseInt(second[1])){
//
//            yy = parseInt(first[1]) + 60 - parseInt(second[1]);
//            xx = parseInt(first[0]) + 24 - 1 - parseInt(second[0]);
//
//        }else{
//            yy = parseInt(first[1]) - parseInt(second[1]);
//            xx = parseInt(first[0]) + 24 - parseInt(second[0]);
//        }
//
//
//
//    }else if(parseInt(first[0]) == parseInt(second[0])){
//
//        if(parseInt(first[1]) < parseInt(second[1])){
//
//            yy = parseInt(first[1]) + 60 - parseInt(second[1]);
//            xx = parseInt(first[0]) + 24 - 1 - parseInt(second[0]);
//
//        }else{
//            yy = parseInt(first[1]) - parseInt(second[1]);
//            xx = parseInt(first[0]) - parseInt(second[0]);
//        }
//
//    }else{
//
//
//        if(parseInt(first[1]) < parseInt(second[1])){
//
//            yy = parseInt(first[1]) + 60 - parseInt(second[1]);
//            xx = parseInt(first[0]) - 1 - parseInt(second[0]);
//
//        }else{
//            yy = parseInt(first[1]) - parseInt(second[1]);
//            xx = parseInt(first[0]) - parseInt(second[0]);
//        }
//
//
//    }
//
//    return(xx + ":" + yy);
//}

// Unused, commenting out for now
//function firstIsBefore(field,rules,i,option){
//    var startTime = parseInt($('#start_time').val());
//    var endTime = parseInt($('#end_time').val());
//
//    if(startTime != "" && endTime != "" && startTime > endTime){
//        return ("The end time must be later than the start time.");
//    }
//}