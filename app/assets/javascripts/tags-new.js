$(document).ready(function(){
    submitButton = $('.submit');
    submitButton.attr('disabled', false);

    //When the form successfully submits disable to button to prevent overzealous users from multi submitting.
    $('#tagForm').on('submit',function(){
        submitButton.attr('disabled', true);
        submitButton.hide('blind');
    });

});