/**
 * Created with JetBrains RubyMine.
 * User: danielward
 * Date: 2/28/13
 * Time: 12:53 PM
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function() {
    $.getJSON(base_api_url + "api/v1/species.json", "", function(data) {
        var html = "";

        console.log("got some stuff");

        $.each(data["items"],function(index, value) {
            html += '<a href="#" class="unit">';
            html += '<div class="col1-4">';
            html += '<div class="imgBlock"><img src="' + value.image_url + "\"border=0 /></div>";
            html += '</div>';
            html += '<div class="col2-4">' + value.name + "&quot;" + value.common_name + "&quot;</div>";
            html += '<div class="col3-4">' + value.proper_name + "</div>";
            html += '<div class="col4-4">' + value.other_name + "</div>";
            html += "</a>"
        });

        $(".species-insertion").append(html);
    });
});
