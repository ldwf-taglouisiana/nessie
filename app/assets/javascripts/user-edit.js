$(document).ready(function(){
    $('form').validationEngine('attach',{promptPosition : "centerRight",  prettySelect: true});

    // When the user clicks 'Change Password', the fields for the password are shown,
    // and the 'Change Password' button is hidden.
    $('.change-password-button').on('click', function(){
        $('.sitePreferences').show('blind');
        $(this).hide('blind');
    });
});