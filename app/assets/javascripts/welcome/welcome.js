// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
//You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
var odometerOptions = { auto: false };
//variable to help us not init the map again
var mapInit = false;

$(document).ready(function () {
  initNews();
  initPhotoStream();
  initWebsocket();
  initClickEvents();

  $('.card-toggle-button').click(function(){
    flipCard();
  });

  $('.odometer').each(function(){
    var theme = $(this).hasClass('minimal') ? 'minimal' : 'plaza';
    var count = parseInt($(this).text());

    var odometer = new Odometer({ el: $(this)[0], value: count, theme: theme });
    odometer.render();
  });

  $('.odometer.odometer-auto-theme .odometer-digit .odometer-digit-inner, .odometer.odometer-theme-minimal .odometer-digit .odometer-digit-inner').css({overflow: 'none'});



  $('.nano').nanoScroller();
});

function initClickEvents(){
  $('.stats-icon').click(function(){
    $('.page-item .stats').show();
    $('.page-item .main-map').hide();
  });

  $('.tags-icon').click(function(){

    $('.page-item .stats').hide();
    $('.page-item .main-map').show();

    //Unfortuantely we have to init the map after the containing div is shown so that the map can have meaningfull sizing etc.
    //if we haven't initialized the map before
    if(!mapInit) {

      //Init the map
      initMap();
      //Bind the map's form
      bindForm();

      setTimeout($('.container form').submit(), 2000);
      mapInit = true;
    }
  });

  $('.alternate-fields button').click(function(){
    // remove the 'selected' class from all the alternate field sets in the group
    $(this).closest('.alternate-group').find('.alternate-fields').removeClass('not-selected');

    // add the 'selected' class to 'this' parent field set
    $(this).closest('.alternate-fields').addClass('not-selected').hide();
    $(this).closest('.alternate-group').find('.alternate-fields:not(".not-selected")').show();
  });
}

function initWebsocket() {
  var dispatcher = new WebSocketRails( base_url + '/websocket');

  var channel = dispatcher.subscribe('stats_update');

  channel.bind('update', function(post) {
    updateStats(JSON.parse(post));
  });
}

function updateStats(data){
  $('.capture-count').text(data.total_captures.count);
  $('.recapture-count').text(data.total_recaptures.count);
}

function initPhotoStream(){
  $.getJSON('/api/photos', function(data){

    var list = $('.flexslider ul');

    $.each(data.items, function(o, item){
      var img = $('<img />').attr({'src': item.api_image_url, 'class': 'flexslider-image'});

      list.append($('<li></li>').append(img));
    });

    initCarousel();
  });
}

function initNews() {
  $('.news-title').click(function(event){
    event.preventDefault();

    var m = $(this).closest('.news-item');
    $('#news_modal').find('.modal-title').html(m.find('.news-title').html());
    $('#news_modal').find('.modal-body').html(m.find('.news-body').html());

    $('#news_modal').modal('show')
  });
}

function initCarousel() {
  $('.flexslider').flexslider({
    animation: "slide",
    itemWidth: 600
  });
}

var map;
var layer;

function initMap() {
  layer = L.geoJson([]);

  var latitude = 29.692825;
  var longitude = -90.044769;

  var worldMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer", {
    detectRetina: true,
    attribution: "Sources: Esri, DeLorme, GEBCO, NOAA NGDC, and other contributors"
  });

  var oceanMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer", {
    detectRetina: true,
    attribution: "Sources: Esri, DigitalGlobe, Earthstar Geographics, CNES/Airbus DS, GeoEye, USDA FSA, USGS, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community"
  });

  var mapMarkings = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer", {
    detectRetina: true,
    attribution: "Sources: Esri, GEBCO, NOAA, National Geographic, DeLorme, HERE, Geonames.org, and other contributors"
  });

  map = new L.Map('homepage_map', {
    center: [latitude, longitude],
    zoom: 8,
    layers: [worldMap]
  });

  var baseMaps = {
    "World Map": worldMap,
    "Ocean Map": oceanMap
  };

  var overlayMaps = {
    "Markings" : mapMarkings
  };

  L.control.layers(baseMaps, overlayMaps).addTo(map);

}

//---------------------------------------------------
//---------------------------------------------------
// map helper functions


function createShapeMap(data) {
  clearMap();

  // Bind a popup to each basin, showing the basin's name and number of captures
  function onEachFeature(feature, layer) {
    feature.properties.popupContent = feature.properties.name + '<br> Captures: ' + feature.properties.count;
    layer.bindPopup(feature.properties.popupContent, {className: 'leaflet-popup-content-wrapper-black-text'});
  }

  if (data.features != null) {

    var colors = [];

    $.each(data.features, function(){
      colors.push(rainbow());
    });

    layer = L.geoJson(data, {
      style: function () {
        var c = colors.pop();

        return {
          fillColor: c,
          color: c
        }
      },
      onEachFeature: onEachFeature
    }).addTo(map);

    map.fitBounds(layer.getBounds(), {animate: false});
  }
}

function clearMap() {
  map.removeLayer(layer);
}

function bindForm(){
  //this code ties the map to the basin maps and adds the captures to it
  //this binds the submit event for the map so it is part of the init phase
  $('.container form').submit(function(){

    //waiting indicator shows up
    var overlay = $(this).closest('.card').find('.progress-overlay');

    overlay.show();

    //has something to do with building the query string to get the capture and basin data
    if ($(this).find('.date-last-fields').is(":hidden")) {
      $(this).find('[name="number_of_days"]').addClass('ignore');
    }

    if ($(this).find('.date-range-fields').is(":hidden")) {
      $(this).find('[name="start_date"]').addClass('ignore');
      $(this).find('[name="end_date"]').addClass('ignore');
    }

    //serialize the form elements so that we can send the data in the request
    var formData = $(this).find('.form-control').not('.ignore').serialize();
    // Users can't change the map type anymore, so set the map type to Basin automatically
    formData = "map_type=Basin&" + formData;

    $.getJSON('/api/capture_map_data', formData, function(data){
      //use the returned info to add the shapes to the map
      createShapeMap(data.json);
      //we can hide the progress indicator
      overlay.hide();
      $('.map-progress-overlay').hide();

      if (isBackShowing()) {
        flipCard();
      }

    }).fail(function(jqXHR, status, error){
      alert('Could not get data from server');
      overlay.hide();

      $('.map-progress-overlay').hide();
      if (isBackShowing()) {
        flipCard();
      }

    });

    $('.ignore').removeClass('.ignore');

    return false;
  });



}



//---------------------------------------------------
//---------------------------------------------------
// global helper functions

function flipCard() {
  $('.front').toggleClass('show_front');
  $('.back').toggleClass('show_back');
}

function isBackShowing() {
  return $('.back').hasClass('show_back');
}

function rainbow() {
  var r, g, b;
  var h = Math.seededRandom ();
  var i = ~~(h * 6);
  var f = h * 6 - i;
  var q = 1 - f;
  switch (i % 6) {
    case 0:
      r = 1, g = f, b = 0;
      break;
    case 1:
      r = q, g = 1, b = 0;
      break;
    case 2:
      r = 0, g = 1, b = f;
      break;
    case 3:
      r = 0, g = q, b = 1;
      break;
    case 4:
      r = f, g = 0, b = 1;
      break;
    case 5:
      r = 1, g = 0, b = q;
      break;
  }
  var c = "#" + ("00" + (~~(r * 255)).toString(16)).slice(-2) + ("00" + (~~(g * 255)).toString(16)).slice(-2) + ("00" + (~~(b * 255)).toString(16)).slice(-2);

  return (c);
}

// the initial seed
Math.seed = 234342;

// in order to work 'Math.seed' must NOT be undefined,
// so in any case, you HAVE to provide a Math.seed
Math.seededRandom = function(max, min) {
  max = max || 1;
  min = min || 0;

  Math.seed = (Math.seed * 9301 + 49297) % 233280;
  var rnd = Math.seed / 233280;

  return min + rnd * (max - min);
}

String.prototype.trunc = String.prototype.trunc ||
function (n) {
  return this.length > n ? this.substr(0, n - 1) + '&hellip;' : this;
};