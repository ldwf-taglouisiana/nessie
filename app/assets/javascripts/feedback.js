var siteID=77;
var scriptBaseURL='http://clientservices.covalentlogic.com/public';

function openFeedbackForm() {
  var referrerURL = document.referrer;
  var pageURL = window.location.href;
  
  var targetURL = scriptBaseURL + "/?md=feedback&tmp=home&sid=" + siteID + "&page=" + escape(pageURL) + "&ref=" + escape(referrerURL);
  
  openWindow(targetURL,"feedback",395,475);
}

function basename (path, suffix) {
    // Returns the filename component of the path  
    // 
    // version: 909.322
    // discuss at: http://phpjs.org/functions/basename
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ash Searle (http://hexmen.com/blog/)
    // +   improved by: Lincoln Ramsay
    // +   improved by: djmix
    // *     example 1: basename('/www/site/home.htm', '.htm');
    // *     returns 1: 'home'
    var b = path.replace(/^.*[\/\\]/g, '');
    
    if (typeof(suffix) == 'string' && b.substr(b.length-suffix.length) == suffix) {
        b = b.substr(0, b.length-suffix.length);
    }
    
    return b;
}

function getScriptPath(scriptname){
    // Check document for our script
	scriptobjects = document.getElementsByTagName('script');
	for (i=0; i<scriptobjects.length; i++) {
		if(basename(scriptobjects[i].src).indexOf(scriptname) == 0){
			// we found our script.. lets get the path
			return scriptobjects[i].src.substring(0, scriptobjects[i].src.lastIndexOf('/'));
		};
	}
	return "";
}
 
function openWindow (url, winname, w, h) {
	var nScrW = 800;
	var nScrH = 600;
	if (screen) {
	  nScrW = screen.availWidth;
	  nScrH = screen.availHeight;
	}	
	var leftPos = (nScrW-w)/2;
	var topPos = (nScrH-h)/2;
	window.open (url, winname, 'width=' + w + ',height=' + h + ',menu=no,toolbars=no,status=yes,scrollbars=yes,resizable=yes,top=' + topPos + ',left=' + leftPos);
	return;  
}