  function submitForm(form) {
	var hasErrors = false;
	$("input, select").each(function(){
	  if ($(this).hasClass("error")) $(this).removeClass("error");
	});

	var requiredFields = ["firstName", "lastName", "email", "shirtSize", "username", "password", "password2"];
	for (var i = 0; i < requiredFields.length; i++) {
	  var value = $("#" + requiredFields[i]).val();
	  if (value == "" || value == "0") {
		hasErrors = true;
		$("#" + requiredFields[i]).addClass("error");
	  }
	}		
	
	if (hasErrors) {
	  alert("The highlighted fields are required.");
	  return;
	} else {	 
	  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;	  
	  if (!emailPattern.test(form.email.value)) {
	  	alert("Please enter a valid email address.");
		form.email.focus();
		return;
	  }
	  
	  if (form.username.value.length < 6 || form.username.value.indexOf(" ") != -1) { 
		alert("Please select a username with at least 6 characters with no spaces.");
		form.username.focus();
		return;		
	  }
	  
	  if (form.password.value.length < 6 || form.password.value.indexOf(" ") != -1) { 
		alert("Please select a password with at least 6 characters with no spaces.");
		form.password.focus();
		return;		
	  }
	  
	  if (form.password.value != form.password2.value) {
		alert("The passwords you entered do not match.");
		form.password.focus();
		return;	
	  }
	  
	  //alert("Form is submitted.");
	  //return;
	  form.submit();
	}
  }
  
  function suggestUsername(form) {
	  if (form.firstName.value != "" && form.lastName.value != "" && form.username.value == "") {
	    var suggestedUsername=form.firstName.value.charAt(0) + form.lastName.value;
		form.username.value=suggestedUsername.toLowerCase();
	  }
  }
  
  function checkPhoneNumber(obj) {
	var str = obj.value;
	for (var i = 0; i < obj.value.length; i++) {
	  var ch = 	obj.value.charAt(i);
	  if (ch < "0" || ch > "9") {
		alert("Please enter only numbers, i.e. 2253456789");
		return;
	  }
	}
  }