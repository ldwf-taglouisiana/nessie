$(document).ready(function(){
    MM_preloadImages('/assets/imgIconShareO.gif','/assets/imgIconFlickrO.gif','/assets/imgIconYouTubeO.gif','/assets/imgIconTwitterO.gif','/assets/imgIconFacebookO.gif','/assets/imgIconRssO.gif','/assets/imgIconPrintO.gif');

    initMap();
    detailInit();
    capturesInit();

    $('.download-captures .download').click(function () {
        window.open('/my_captures_report.xlsx', 'window name', 'window settings');
    });

    $('.datepicker').datepicker();

    $('.carousel').on('slid.bs.carousel', function () {
        //selector to find the new current slide so we can do stuff to it.
        var selector = $(this).find('.active');
        //if the slide has a map already, don't load it again
        if(selector.find('img').length <= 1){
            //if it doesn't have a map get one
            selector.find('.col2-2').append('<img class="static-map" height="200" width="300" src=' + selector.find('.map-url').val() + '>');
        }

        //get the unit part of the current slide
        var unitSelector =  selector.find('.unit');
        //unbind its clicks so that we can safely rebind them, this prevents double click events
        unitSelector.unbind('click');
        unitSelector.click(function(){
            //when the unit is clicked
            $('.detailExpansion').hide('blind');
            $('.carousel-control').hide('');
            var detail = $(this).closest('.capture-container').find('.detailExpansion');
            var control =  $(this).closest('.capture-container').find('.carousel-control');
            var carousel = $(this).closest('.capture-container').find('.carousel-inner');
            // if the detail is visible, hide it
            if (detail.is(":visible")) {
                detail.hide('blind');
                control.hide('');
            } //otherwise show it
            else {
                detail.show('blind');
                control.show('');
            }
            return false;
        });
    });
});

var markerCluster;
function capturesInit(){
    if ( $('.capture-dashboard-page').length != 0 ){

        var lats = [];
        var longs = [];
        var units = [];

        $('.unit').each(function (index) {
            var lat = $(this).find('.latitude').val();
            var lon = $(this).find('.longitude').val();
            longs.push(lon);
            lats.push(lat);
            units.push($(this))
        });

        var markergroup = new L.LayerGroup();

        for (var i = 0; i < lats.length; i++) {
            if (!isNaN(parseFloat(lats[i])) && !isNaN(parseFloat(longs[i]))) {

                var image = "/assets/imgMapPointerCapture.png";

                var m = createMarkerWithWindowAt(lats[i], longs[i], image, units[i], 'capture');
                m.addTo(markergroup);
            }
        }
    }

    markerCluster = new L.MarkerClusterGroup();
    markerCluster.addLayer(markergroup);
    map.addLayer(markerCluster);
}

//TODO why there is a size difference between frames on the carousel
function detailInit(){
    $('.unit').click(function(){
        $('.detailExpansion').hide('blind');
        $('.carousel-control').hide('');
        var selector = $(this).closest('.capture-container').find('.detailExpansion');
        var control =  $(this).closest('.capture-container').find('.carousel-control');
        var carousel = $(this).closest('.capture-container').find('.carousel-inner')

        //if the collapsable view is visible, hide it,
        if (selector.is(":visible")) {
            selector.hide('blind');
            control.hide('');
        } else {   //otherwise show it, but also maybe populate new slides if there are recaptures

            //if the map has already been drawn don't draw it again, otherwise draw the map
            if(carousel.find('img').length <= 1){
                selector.find('.col2-2').append('<img class="static-map" src=' + selector.find('.map-url').val() + '>');
            }

            //This is a look up of the relevant recaptures for this tag number only fires if the tag has associated recaptures
            if (carousel.children().length == 1 && carousel.find('.has-recapture').val() == 'true' ){
                var tag = $(this).closest('.capture-container').find('.tag_number').val();
                var fishPic = $(this).closest('.capture-container').find('.fish-holder').html();

                //ajax call to a route to get the info
                $.getJSON( '/api/tag_history.json?tag_number=' + tag, function( data ) {
                    //pare the returned data
                    $.each( data.items, function( key, val ) {

                        //some timestamp fiddling to get it localized and human readable
                        var time = new Date(val.capture_date.split('T')[0]).toDateString().split(' ');
                        var timeString = time[1] +' '+ time[2] + ', ' + time[3];

                        //build the path to the relevant speci
                        var speciesString = "/species/" + val.species_id;


                        //append more html into the carousel to create new slides
                        var latInfo = '';
                        if(val.latitude != null){
                            latInfo = '<h4>Lat/Long</h4>' +
                                '<p>'+val.latitude+'/'+val.longitude+'</p>';
                        }


                        var html = '<div class="item">' +
                            '<a href="#" id='+val.id+' class="unit" style="padding: 0px">' +
                            '<div class="col1-4">'+timeString+'</div>' +
                            '<div class="col2-4">'+val.species_name+'</div>' +
                            '<div class="col3-4">'+val.tag_number+'</div>' +
                            '<input class="tag_number" id="tag_number" name="tag_number" type="hidden" value="'+val.tag_number+'" />' +
                            '<input class="species_name" id="species_name" name="species_name" type="hidden" value="'+val.species_name+'" />' +
                            '<input class="capture_date" id="capture_date" name="capture_date" type="hidden" value="'+timeString+'" />' +
                            '<input class="recapture" id="recapture" name="recapture" type="hidden" value="'+val.recapture+'" />' +
                            '<div class="col4-4"></div>' +
                            '<div class="detailExpansion">' +
                            '<div class="col1-2">' +
                            '<h3>Recapture Details</h3>' +

                            '<h4>Tag Number</h4>' +
                            '<p>'+val.tag_number+'</p>' +

                            '<h4>Date</h4>' +
                            '<p>'+timeString+'</p>';

                        if( val.time_of_day_option ) {
                            html += '<h4>Time of Day</h4>' +
                                '<p>'+val.time_of_day_option+'</p>';
                        }

                        html += '<h4>Fish Species</h4>' +
                            '<p>'+val.species_name+'<br />' +
                            '<a href='+speciesString+' class="species-link">Learn More About this Species »</a>';

                        if( val.length ) {
                            html += '<h4>Length</h4>' +
                                '<p>'+val.length+'</p>';
                        }

                        if( val.fish_condition ) {
                            html += '<h4>Fish Condition</h4>' +
                                '<p>'+val.fish_condition+'</p>';
                        }

                        html += latInfo;

                        if( val.location_description ) {
                            html += '<h4>Location Description</h4>' +
                                '<p>'+val.location_description+'</p>';
                        }

                        if( val.comments ) {
                            html += '<h4>Comments</h4>' +
                                '<p>'+val.comments+'</p>' ;
                        }

                        html += '</div>' +
                            //This crazy line here is most of the static map gen from google, there are a couple of places that change
                            //that's what the val parts are for.
                            '<input id="map_url" class="map-url" type="hidden" value='+val.map_url+' name="map_url">' +

                            '<div class="col2-2"><div class="fish-holder">' +
                            fishPic +
                            '</div></div>' +
                            '</div>' +
                            '</a>';

                        carousel.append(html);
                    });
                });

                //carousel will auto slide unless it is paused
                $('.carousel').carousel('pause');
                control.show('');
            }

            //show the hidden view
            selector.show('blind');

        }
        return false;

    });
}

function countOf (array, item) {
    var count = 0;
    for (var i = 0; i < array.length; i++) {
        if (array[i] == item) {
            count++;
        }
    }
    return count;
}
