$(function(){
  $powerBar=$('#powerBar');
  $powerBarMinimized=$('#powerBarMinimized');
  $btnPowerBar=$('#btnPowerBar');
  
  $('#lnkPowerBar').click(function(e){
	e.preventDefault();
	$divMinMaxTab=$('#divMinMaxTab');
	if ($divMinMaxTab.hasClass('minimized')) {
	  //need to maximize
	  $divMinMaxTab.removeClass('minimized');
	  $powerBar.show();
	  $powerBarMinimized.hide();
	  $btnPowerBar.attr('src','/images/btnPowerBarMinimize.png');
	  
	  $.cookie('powerBarDisplayMode', 'max', { path: '/' });
	} else {
	  //need to minimize
	  $divMinMaxTab.addClass('minimized');
	  $powerBarMinimized.show();
	  $powerBar.hide();
	  $btnPowerBar.attr('src','/images/btnPowerBarMaximize.png');
	  
	  $.cookie('powerBarDisplayMode', 'min', { path: '/' });
	}
  });
});