// Following function used to autofill username with value in email field.
// Username field is gone, so this method is commented out.
//window.onload = function () {
//    var first = document.getElementById('email'),
//        second = document.getElementById('username');
//
//    first.onkeyup = function () { // or first.onchange
//        second.value = first.value;
//    };
//};

$(document).ready(function(){
    $('form').validationEngine('attach',{
        promptPosition : "centerRight",
        prettySelect: true,
        focusFirstField: false
    });
});