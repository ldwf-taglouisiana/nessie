/**
 * Created with JetBrains RubyMine.
 * User: danielward
 * Date: 3/6/13
 * Time: 12:30 PM
 * To change this template use File | Settings | File Templates.
 */

/* global variables */
var increment = 0;
var maps = [];
var shouldAddCard = false;
var addCardValidation = "addCardValidation";
var cardValidationComplete = "cardValidationComplete";

var coordinateMasks = {
    D:    '-9{1,2}.[9{1,4}]°',
    DM:   '-9{1,2}° 9{1,2}.[9{1,4}]\'',
    DMS:  '-9{1,2}° 9{1,2}\' 9{1,2}.[9{1,2}]"'
};

$(document).ready(function(){
    MM_preloadImages('/assets/imgIconShareO.gif','/assets/imgIconFlickrO.gif','/assets/imgIconYouTubeO.gif','/assets/imgIconTwitterO.gif','/assets/imgIconFacebookO.gif','/assets/imgIconRssO.gif','/assets/imgIconPrintO.gif');

    initMap($('.map'));

    initAddCardButton();

    $('#date_0').datepicker();

    $('.real-submit').click(function(){
        $('.submit').attr('disabled', true);
        $('#captureForm').submit();
    });

    $('.uuid').val(generateUUID());

    // enable the validations on the form
    attachValidation();
    //attach every click and focus event etc
    initEvents();

    initDraftEdit();

});

/* Init functions */
function initMap(selector) {
    var parent =  selector.closest('.capture-card');

    var latitude = 29.692825;
    var longitude = -90.044769;

    var worldMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer", {
        detectRetina: true,
        attribution: "Sources: Esri, DeLorme, GEBCO, NOAA NGDC, and other contributors"
    });

    var oceanMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer", {
        detectRetina: true,
        attribution: "Sources: Esri, DigitalGlobe, Earthstar Geographics, CNES/Airbus DS, GeoEye, USDA FSA, USGS, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community"
    });

    var mapMarkings = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer", {
        detectRetina: true,
        attribution: "Sources: Esri, GEBCO, NOAA, National Geographic, DeLorme, HERE, Geonames.org, and other contributors"
    });

    var map = new L.Map(selector[0], {
        center: [latitude, longitude],
        zoom: 8,
        layers: [worldMap]
    });

    var baseMaps = {
        "World Map": worldMap,
        "Ocean Map": oceanMap
    };

    var overlayMaps = {
        "Markings" : mapMarkings
    };

    L.control.layers(baseMaps, overlayMaps).addTo(map);

    function onMapClick(e) {
        var m = findMap(map);
        updateCoordinates(parent, e.latlng);

        // remove the layers on the map
        for(var i = 0; i < m.layers.length; i++){
            m.map.removeLayer(m.layers[i]);
        }

        var marker = new L.marker(e.latlng);

        marker.addTo(m.map);
        m.layers.push(marker);
    }

    map.on('click', onMapClick);

    maps.push({map: map, layers:[]});

    parent.find('.map').find('a').attr('tabindex', '-1');

    updateMap(selector);
}

function initDeleteButton(cardSelector) {
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    cardSelector.find('.delete-card-button').click(function(){
        var card = $(this).closest('.capture-card');

        var removeCard = function () {
            card.remove();

            var numberOfCards = $('.capture-card').length;
            if(numberOfCards == 1) {
                $('.capture-card')
                    .find('.chevron, .button-row').hide();

                openCard($('.capture-card'));
            }
        };

        var properties = {
            opacity: 0,
            height: '0px'
        };

        var options = {
            duration: 700,
            complete: removeCard
        };

        card.animate( properties, options);
    });
}

function initAddCardButton() {

    var form = $('#captureForm');
    var addCardButton = $('.add-capture-button');
    var addCard = function addCard() {

        // update the increment counter
        increment = increment + 1;

        // get the last card in the DOM
        var lastCard = $('.capture-card').last();

        // copy the last card
        var temp = lastCard.clone();

        // reset values that were not indicated as locked

        //PROBLEM HERE
        temp.find('.unlocked').val('');

        //set the uuid of the new card to a new uuid
        temp.find('.uuid').val(generateUUID());

        // Set recapture value to false if unlocked.
        temp.find('.recaptureValue.unlocked').val('false');

        // GPS type does not have a blank default value, so set it to 'D' instead
        temp.find('.gps-type').val('D');

        // adjust the ids and names of the form elements
        temp.find('.field').each(function(){
            // update the id with the current increment value
            var idParts = $(this).attr('id').split('_');
            var newId = idParts[0] + '_' + increment;
            $(this).attr('id', newId);

            if ($(this).attr('name')) {
                // update the captures index with the current increment value
                var m = $(this).attr('name').replace(/(\[)([0-9]+)(\])/, function(match, capture1, capture2 ,capture3){
                    // capture2 is unused
                    return capture1 + increment + capture3;
                });

                // assign the updated value to the name attribute
                $(this).attr('name', m);
            }
        });

        // update the recapture switch label
        var recaptureSwitch = temp.find('.onoffswitch-label');
        recaptureSwitch.attr('for', (recaptureSwitch.attr('for').split('_')[0] + '_' + increment) );

        // increment the tag number
        var tagNumberField = temp.find('.tagNumberField');
        var newTagNumber = incrementTagNumber(tagNumberField.val());

        tagNumberField.val(newTagNumber);

        temp.find('.tag-number-label').text(newTagNumber);

        //increment the tab indices on the new card
        incrementTabIndices(temp);

        // collapse the previous card
        collapseCard(lastCard);

        // insert the new card
        $(temp).insertAfter(lastCard);

        // enable the chevrons to show/hide cards
        $('.chevron').show();

        // enable the delete buttons
        $('.capture-card').find('.button-row').show();

        // reference for the temp card written to the DOM
        var newCard = $('.capture-card').last();

        // focus on the tag number field on the new card
        newCard.find('.tagNumberField').focus();

        // updated the selected select fields and textarea fields
        var lastFields = lastCard.find('select.locked');
        lastFields = lastFields.add(lastCard.find('textarea.locked'));
        var newFields = newCard.find('select.locked');
        newFields = newFields.add(newCard.find('textarea.locked'));
        for (var i = 0; i < lastFields.size(); i++) {
            newFields[i].value = lastFields[i].value;
        }

        // update the recapture checkbox
        newCard.find('input.unlocked.recaptureField').prop('checked', false);


        // Here we check the values of the recapture check box and the species and recapture
        //  disposition so that only the correct boxes get displayed on the new card when they're supposed to
        var disField =  newCard.find('.recaptureDispositionFieldContainer');
        var conField =  newCard.find('.fishConditionContainer');
        var disFieldVal = disField.find('.dispositionField').val();

        if(newCard.find('.recaptureField').is(':checked')) {
            disField.show();

            if (disFieldVal == '3') {
                conField.hide();
            }

        }else {
            disField.hide();
            conField.show();
        }

        initEvents(newCard);
        // add the input masks back
        // remove and reapply validations
        toggleValidation();

        // initialize the map for the new card
        initMap(newCard.find('.map'));
        updateMap(newCard);

        // have to remove the class that datepicker uses to identify datepicker fields from the new field and then
        // re attach it. This is because the clone copied the class but not the datepicker bind, but datepicker can't
        // re-bind unless the field does not have this class already.
        newCard.find('.datepicker').removeClass('hasDatepicker');
        $('.datepicker').datepicker();
    };

    addCardButton.click(function(){
        addCard();
    });
}

function initEvents(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    initGpsChange(cardSelector);
    initTagOut(cardSelector);
    initRecapChange(cardSelector);
    addInputMasks(cardSelector);
    initMaskFocus(cardSelector);
    //initSpeciesFieldChange(cardSelector);
    initDispositionField(cardSelector);
    initDropDownButtons(cardSelector);
    initDeleteButton(cardSelector);
    initLockButtons(cardSelector);
    initFractionConversion(cardSelector);
    toggleToolTips();

}


//Init binding functions
function initGpsChange(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');
    var gpsSelector = cardSelector.find('.gps-type');

    gpsSelector.change(function(){

        cardSelector.find('.coordinate_mask').each(function() {
            var gpsType = gpsSelector.val();

            var mask = $(this).hasClass('latitude-entry') ? coordinateMasks[gpsType].replace('-9{1,2}', '9{1,2}') : coordinateMasks[gpsType];
            $(this).inputmask({ mask: mask, greedy: false });
        });
        updateCoordinates(cardSelector);
    });
}

function initTagOut(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    cardSelector.find('.tagNumberField').focusout(function() {
        // change the case of the tag number
        $(this).val($(this).val().toUpperCase());

        // update the label in the header with the tag number
        $(this).closest('.header').find('.tagNumber-label').text($(this).val());
    }).focus();
}


function initRecapChange(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    var field = cardSelector.find('.recaptureField');

    field.change(function() {
        cardSelector.find('.recaptureValue').val(
            $(this).is(':checked')
        );
        var disField =  cardSelector.find('.recaptureDispositionFieldContainer');
        var conField =  cardSelector.find('.fishConditionContainer');
        var disFieldVal = disField.find('.dispositionField').val();


        //have to think about how to check for fish condition and recap stuff
        //the only time fish condition should
        if($(this).is(':checked')) {
            disField.show();

            if (disFieldVal == '3') {
                conField.hide();
            }

        }else {
            disField.hide();

            if (disFieldVal == '3') {
                conField.hide();
            }
        }
    });
}


function initMaskFocus(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    cardSelector.find('.coordinate_mask').focusout(function(){
        $(this).closest('.coordinate-group').find('.decimal').val(coordStringToDecimal($(this).val()));
        updateMap($(this).closest('.capture-card'));
    });
}


function initSpeciesFieldChange(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    //Show species length options if the species chosen has length ranges
    cardSelector.find('.speciesField').change(function() {
        var speciesField = $(this);
        var speciesLengthRow = $(this).closest('.capture-card').find('.species-length-row');

        var options = speciesLengthRow.find(".length-prison option[species_id='" + speciesField.val() +"']");

        if (options.length == 0) {
            speciesLengthRow.val('');
            speciesLengthRow.hide('blind');
        } else {
            speciesLengthRow.find('select').empty().append(options.clone());
            speciesLengthRow.show('blind');
        }
    });
}

function initDispositionField(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    // Show/hide the fish condition field depending on the recapture disposition
    // Can't tell how a fish was upon release if you never released it.
    cardSelector.find('.dispositionField').change(function() {
        var disposition = $(this).val();
        var conditionField = cardSelector.find('.fishConditionContainer');

        if (disposition == '3') {
            conditionField.hide('blind');
        }else {
            conditionField.show('blind');
        }
    });
}


function initDropDownButtons(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    // add the click event for the header to open/close it
    cardSelector.find('.header .chevron').click(function(){
        toggleCard(cardSelector);
    });
}


function initLockButtons(cardSelector) {
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    cardSelector.find('.lock-field-button').click(function(){
        if ($(this).find('i').hasClass('fa-unlock')){
            // add the lock to the field
            $(this).find('i').removeClass('fa-unlock');
            $(this).find('i').addClass('fa-lock');

            $(this).closest('.row').find('.unlocked').removeClass('unlocked').addClass('locked');
        } else {
            //remove the lock to a field
            $(this).find('i').removeClass('fa-lock');
            $(this).find('i').addClass('fa-unlock');

            $(this).closest('.row').find('.locked').removeClass('locked').addClass('unlocked');
        }
    });
}

function initDraftEdit() {
    $('.recaptureValue').each(function(){
        if ($(this).val() == 'true') {
            $(this).closest('.capture-card').find('.recaptureField').attr('checked','checked');
        }
    });

    if ($('.recaptureField').is(':checked')) {
        $('.recaptureDispositionFieldContainer').show();
    }

    // If the map is not at the default values (0 lat and 0 long),
    // put a marker where the capture's coordinates are
    if ($('.latitude').val() != 0 && $('.longitude').val() != 0) {
        updateMap($('.capture-card'));
    }

    // Change the species field to activate showing/hiding of the species length field
    $('.speciesField').change();
}

function initFractionConversion(cardSelector){
    if (typeof cardSelector == "undefined")
        cardSelector = $('.capture-card');

    cardSelector.find('.length').focusout(function(){
        $(this).val(fractionToDecimal($(this).val()));
    });

}

/* validation functions */
function validateLatitude(field, rules, i, options){
    var regex = /^((\d{0,2}(\.\d{0,4}){0,1})°)((\s(\d{0,2}(\.\d{0,2}){0,1})')(\s((\d{0,2}(\.\d{0,2}){0,1})")){0,1}){0,1}/ ;

    var matches = field.val().match(regex);
    if (matches == null){
        return "Latitude must not be empty";
    }

    var decimal = parseFloat(matches[2]);
    var minutes = parseFloat(matches[6]);
    var seconds = parseFloat(matches[10]);

    if (!isNaN(decimal) && (decimal < 0 || decimal > 60)) {
        return "Decimal value must be between 0 and 60";
    }

    if (!isNaN(minutes) && (minutes < 0 || minutes > 60)) {
        return "Minutes value must be between 0 and 60";
    }

    if (!isNaN(seconds) && (minutes < 0 || minutes > 60)) {
        return "Seconds value must be between 0 and 60";
    }
}

function validateLongitude(field, rules, i, options) {
    var regex = /^((-\d{0,3}(\.\d{0,4}){0,1})°)((\s(\d{0,2}(\.\d{0,2}){0,1})')(\s((\d{0,2}(\.\d{0,2}){0,1})")){0,1}){0,1}/ ;

    var matches = field.val().match(regex);
    if (matches == null){
        return "Latitude must not be empty";
    }

    var decimal = parseFloat(matches[2]);
    var minutes = parseFloat(matches[6]);
    var seconds = parseFloat(matches[10]);

    if (!isNaN(decimal) && (decimal > -60 || decimal < -150)) {
       return "Decimal value must be between -150 and -60";
    }

    if (!isNaN(minutes) && (minutes < 0 || minutes > 60)) {
        return "Minutes value must be between 0 and 60";
    }

    if (!isNaN(seconds) && (minutes < 0 || minutes > 60)) {
        return "Seconds value must be between 0 and 60";
    }
}

/* general helpers  */
function incrementTagNumber(tagNumber) {
    var result = tagNumber.replace(/([0-9]+)/, function(match, number){
        return (Number(number) + 1) + '';
    });

    return result;
}

function addInputMasks(selector) {
    if (typeof selector == "undefined")
        selector = $('.capture-card');

    selector.find('.tagNumberField').inputmask({ mask: 'A[A]999999', greedy: false });

    selector.find('.datepicker').inputmask("99/99/9999");

    selector.find('.coordinate_mask').each(function() {
        var gpsType = $(this).closest('.capture-card').find('.gps-type').val();

        var mask = $(this).hasClass('latitude-entry') ? coordinateMasks[gpsType].replace('-', '') : coordinateMasks[gpsType];

        try {
            $(this).inputmask(
                {
                    mask: mask,
                    greedy: false,
                    nojumps: false
                });
        }
        catch(err) {
        }

    });
}

function removeInputMasks(selector) {

    if (typeof selector == "undefined")
        selector = $('.capture-card');

    selector.find('.tagNumberField').inputmask('remove');

    selector.find('.datepicker').inputmask('remove');

    selector.find('.coordinate_mask').each(function() {
        $(this).inputmask('remove');
    });
}

function toggleValidation() {
    detachValidation();
    attachValidation();
}

function attachValidation() {
    var form = $('#captureForm');

    // will refresh the buttons so they are usable again.
    var refreshButtons = function() {
        $('.btn').removeAttr('disabled');
    };

    var failure = function () {
        refreshButtons();
    };

    form.validationEngine('attach',{
        promptPosition : "centerRight",
        prettySelect: true,
        focusFirstField: false,
        onValidationComplete: function(form, valid){
            if (!valid) {
                failure();
            }
            return valid;
        }
    });
}

function detachValidation() {
    $('#captureForm').validationEngine('detach');
}

function incrementTabIndices(selector) {
    //increment the tab indices on the new card
    var currentIndex = getTabIndex();
    var sortedInputs = selector.find('[tabindex]').not('[tabindex="-1"]').toArray().sort(function sorter(a, b) {
        return a.getAttribute('tabindex') - b.getAttribute('tabindex');
    });

    $.each(sortedInputs, function () {
        $(this).attr('tabindex', currentIndex);
        currentIndex++;
    });
}

function toggleToolTips() {
    $('.lock-field-button').tooltip('destroy').tooltip();
}

function getTabIndex() {
    var max = 1;

    $('.capture-card').find('[tabindex]').not('[tabindex="-1"]').each(function() {
        var index = parseInt($(this).attr('tabindex'));

        if(index > max) {
            max = index;
        }
    });

    return max + 1;
}

/* card animation functions */
function toggleCard(cardSelector) {
    var header  = cardSelector.find('.header');

    // if the card header has the 'collapsed' class then it
    // is already collapsed
    if(header.hasClass('collapsed')){
        openCard(cardSelector);
    } else {
        collapseCard(cardSelector);
    }
}

function openCard(cardSelector) {
    var header  = cardSelector.find('.header');
    var content = cardSelector.find('.content');

    header.removeClass('collapsed');
    header.find('.chevron').removeClass('rotate-up');

    content.show({
        effect: 'blind',
        complete: function() {
            header.find('.show-when-collapsed').hide();
            header.find('.hide-when-collapsed').show();
            header.find('.chevron-container').removeClass('col-md-offset-3').addClass('col-md-offset-4');
        }
    });
}

function collapseCard(cardSelector) {
    var header  = cardSelector.find('.header');
    var content = cardSelector.find('.content');

    header.find('.chevron').addClass('rotate-up');

    header.find('.hide-when-collapsed').hide();
    header.find('.show-when-collapsed').show();
    header.find('.chevron-container').removeClass('col-md-offset-4').addClass('col-md-offset-3');

    content.hide({
        effect: 'blind',
        complete: function(){
            header.addClass('collapsed');

            header.find('.tagNumber-label').text(content.find('.tagNumberField').val());
            header.find('.date-label').text(content.find('.dateField').val());
            header.find('.species-label').text(
                content.find(".speciesField option[value='" + content.find('.speciesField').val() + "']").text()
            );
        }
    });

}

/* Map helpers */
function findMap(map) {
    for(var i = 0; i < maps.length; i++){
        if(maps[i].map === map){
            return maps[i];
        }
    }

    return null;
}

function updateMap(cardSelector) {
    //This means find the index of the cardSelector and on the capture-card not its siblings
    var m = maps[cardSelector.index('.capture-card')];

    if (isLocationEmpty(cardSelector)) {
        return;
    }

    // remove the layers on the map
    for(var i = 0; i < m.layers.length; i++){
        m.map.removeLayer(m.layers[i]);
    }

    var latitude  = cardSelector.find('.latitude').val();
    var longitude = cardSelector.find('.longitude').val();
    var marker = new L.marker(L.latLng(latitude, longitude));

    marker.addTo(m.map);
    m.layers.push(marker);
    m.map.panTo(marker.getLatLng(), { animate: true });
}

function isLocationEmpty(selector) {
    var latitude  = latitudeVal(selector);
    var longitude = longitudeVal(selector);

    return (isNaN(latitude) || isNaN(longitude)) || (latitude == 0 || longitude == 0);
}

function latitudeVal(selector) {
    return parseFloat($(selector).find('.latitude').val());
}

function longitudeVal(selector) {
    return parseFloat($(selector).find('.longitude').val());
}

function updateCoordinates(selector, latlng) {
    //  if latlng was not given
    if (latlng === undefined) {

        //  if the location in the card is invalid then return
        if (isLocationEmpty(selector)){
            return;
        } else {
            latlng = L.latLng(latitudeVal(selector), longitudeVal(selector));
        }
    }

    var la = convertCoordinates(latlng.lat);
    var lo = convertCoordinates(latlng.lng);

    $(selector).find('.latitude-entry').val(la[$(selector).find('.gps-type').val()].string);
    $(selector).find('.longitude-entry').val(lo[$(selector).find('.gps-type').val()].string);
    //seems like the lat and long are always stored in a given way and conversion is just for looks...
    $(selector).find('.latitude').val(latlng.lat);
    $(selector).find('.longitude').val(latlng.lng);
}

function convertCoordinates(value) {

    var decimal = 0;
    var minutes = 0;
    var seconds = 0;
    var string = '';

    var result = {};

    ////////////////////////////////////////////////////////////////
    //    create the decimal degrees portion of the result
    decimal = value.toFixed(4);

    decimal = (Math.abs(decimal) * (decimal / Math.abs(decimal))) || null;

    string = decimal;

    result['D'] = {
        degrees: decimal,
        string: string
    }

    ////////////////////////////////////////////////////////////////
    //    create the decimal minutes portion of the result
    decimal = Math.floor(Math.abs(value));
    minutes = ( Math.abs(value) - Math.abs(decimal) ) * 60;

    decimal = Math.abs(decimal) * (value / Math.abs(value));
    minutes = minutes.toFixed(2);

    string = decimal + "° " + minutes + "'";

    result['DM'] = {
        degrees: decimal,
        minutes: minutes,
        string: string
    }

    ////////////////////////////////////////////////////////////////
    //    create the decimal minutes seconds portion of the result
    decimal = Math.floor(Math.abs(value));
    minutes = ( Math.abs(value) - Math.abs(decimal) ) * 60;
    seconds = ( Math.abs(minutes) - Math.floor(minutes) ) * 60;

    decimal = Math.abs(decimal) * (value / Math.abs(value));
    minutes = Math.floor(minutes);
    seconds = seconds.toFixed(2);

    string = decimal + "° " + minutes + "' " + seconds + "''";

    result['DMS'] = {
        degrees: decimal,
        minutes: minutes,
        seconds: seconds,
        string: string
    }

    return result;
}

//http://bucarotechelp.com/design/jseasy/91090002.asp
//Base code with some modification. Got it from this guy Stephen Bucaro
function fractionToDecimal(inString){
    var StrIn = inString;
    var decimalAnswer;

    if(StrIn.indexOf("/") > -1 && StrIn.match(/^(\d+)((\.\d+)|(\s\d+\/\d+)|())$/) != null){
        var num;
        var den;
        if(StrIn.indexOf(" ") > -1){
            var whole = StrIn.slice(0, StrIn.indexOf(" "));
            var fract = StrIn.slice(StrIn.indexOf(" ") + 1, StrIn.length);

            num = fract.slice(0, fract.indexOf("/"));
            den = fract.slice(fract.indexOf("/") + 1, fract.length);
            decimalAnswer = parseInt(whole) + num/den;
        }
        else{
            num = StrIn.slice(0, StrIn.indexOf("/"));
            den = StrIn.slice(StrIn.indexOf("/") + 1, StrIn.length);
            decimalAnswer = num/den;
        }
    }
    else{
        decimalAnswer = StrIn;
    }
    return decimalAnswer;
}

function coordStringToDecimal(coordString) {
    var clean = coordString.replace(/_/g,'').replace('°', '').replace('\'', '').replace('"', '');

    var components = clean.split(' ');
    var d = parseFloat(components[0]) || 0;
    var m = parseFloat(components[1]) || 0;
    var s = parseFloat(components[2]) || 0;

    return (d/Math.abs(d)) * (Math.abs(d) + (m / 60) + (s / 3600));
}

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};