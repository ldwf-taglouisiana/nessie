
$(window).load(function () {
    initMap();
    MM_preloadImages('/assets/imgIconShareO.gif','/assets/imgIconFlickrO.gif','/assets/imgIconYouTubeO.gif','/assets/imgIconTwitterO.gif','/assets/imgIconFacebookO.gif','/assets/imgIconRssO.gif','/assets/imgIconPrintO.gif');
    reInitMap();
});

$(document).ready(function(){
    marinaInit();
});

function marinaInit(){
    $('.spotName').click(function(){
        $('.detailExpansion').hide('blind');

        if ($(this).closest('.marina-container').find('.detailExpansion').is(":visible")) {
            $(this).closest('.marina-container').find('.detailExpansion').hide('blind');
        } else {
            $(this).closest('.marina-container').find('.detailExpansion').show('blind');
        }

        return false;

    });
}

function reInitMap(){
    var markergroup = new L.LayerGroup();
    $('.marina-container').each(function (index) {

        var lat = $(this).find('.latitude').val();
        var lon = $(this).find('.longitude').val();
//        console.log(parseFloat(lat) + " , " + parseFloat(lon));
        if (!isNaN(parseFloat(lat)) && !isNaN(parseFloat(lon))) {

            var image = "/assets/marina_marker.png";

            var m = createMarkerWithWindowAt(lat, lon, image, $(this), "marina");
            m.addTo(markergroup);
        }
    });
    var markerCluster = new L.MarkerClusterGroup();
    markerCluster.addLayer(markergroup);
    map.addLayer(markerCluster);
}