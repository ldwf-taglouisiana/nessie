$(window).load(function () {
    var latitude = $('.latitude').val();
    var longitude = $('.longitude').val();
    addSingleMarker(latitude, longitude);
    updateLatitude(latitude);
    updateLongitude(longitude);
    selectRecaptureDisposition($('.recapture_disposition_id').val());
});

function selectRecaptureDisposition(dispositionID){
    document.getElementById('disposition').value = dispositionID;
}