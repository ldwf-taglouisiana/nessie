// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require default
//= require websocket_rails/main
//= require other_libs/OverlappingMarkerSpiderfier.js

var map;

$(document).ready(function(){
    $('.phone_number').inputmask("(999) 999-9999");
    $('.zip').inputmask("99999");
});


function initMap() {
    var latitude = 29.692825;
    var longitude = -90.044769;

    var worldMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer", {
        detectRetina: true,
        attribution: "Sources: Esri, DeLorme, GEBCO, NOAA NGDC, and other contributors"
    });

    var oceanMap = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer", {
        detectRetina: true,
        attribution: "Sources: Esri, DigitalGlobe, Earthstar Geographics, CNES/Airbus DS, GeoEye, USDA FSA, USGS, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community"
    });

    var mapMarkings = L.esri.tiledMapLayer("https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer", {
        detectRetina: true,
        attribution: "Sources: Esri, GEBCO, NOAA, National Geographic, DeLorme, HERE, Geonames.org, and other contributors"
    });

    map = new L.Map($('.map')[0], {
        center: [latitude, longitude],
        zoom: 8,
        layers: [worldMap]
    });

    var baseMaps = {
        "World Map": worldMap,
        "Ocean Map": oceanMap
    };

    var overlayMaps = {
        "Markings" : mapMarkings
    };

    L.control.layers(baseMaps, overlayMaps).addTo(map);

}

// Used by maps for marinas and captures
function createMarkerWithWindowAt(lat, lon, image, elem, contentType) {
    var myLatlng = new L.LatLng(lat, lon);

    var id = elem.attr('id');

    var contentString = setContent(id, elem, contentType);

    var infowindow = new L.popup({
        maxWidth: 240,
        maxHeight: 240
    });
    infowindow.setContent(contentString);

    var icon = L.icon({ iconUrl: image});

    var marker = L.marker(myLatlng, { icon: icon });

    marker.bindPopup(infowindow).openPopup();

    return marker;
}

// Set the content inside of a google maps window
function setContent (id, elem, type) {
    var contentString;

    var substring = "scrollToDetails(\"" + id + "\",\"" + type + "\")";

    if (type == "capture") {
      contentString = '<div style="color: #5c6068;">'
          + 'Tag Number: ' + elem.find('.tag_number').val()
          + '<br>Capture Date: ' + elem.find('.capture_date').val()
          + '<br>Species: ' + elem.find('.species_name').val()
          + '<br><a style="color: dodgerblue" onclick=' + substring + ';return false;">Click Here for More Info</a>'
          + '</div>';
    }
    else if (type == "marina") {
        contentString = '<div style="color: #5c6068;">'
            + elem.find('.name').val()
            + '<br> ' + elem.find('.address').val()
            + '<br><a style="color: dodgerblue" onclick=' + substring + ';return false;">Click Here for More Info</a>'
            + '</div>';
    }
    else {
        contentString = 'No content type specified.'
    }
    return contentString
}

// Scroll to the detailExpansion of the capture/marina map marker
function scrollToDetails (id, type) {
    $('.detailExpansion').hide('blind');
    $("html").stop().animate({
        scrollLeft: $("#" + id).offset().left,
        scrollTop: $("#" + id).offset().top
    }, 1200, function(){
        var item = $("#" + id);
        // If this is the capture view, click the unit to fire the js to
        // generate the map inside of the detail view
        if (type == 'capture') {
            item.closest('.unit').click()
        }
        // If this is the marina view, expand the detailExpansion
        else if (type == 'marina') {
            var selector = item.closest('.marina-container').find('.detailExpansion');
            selector.show('blind');
        }
        // Otherwise, log it.
        else {
            console.log("Error: Unsupported type \'" + type + "\'")
        }
    });
}

// site wide function call to check the date for date entries.
function checkDate(field, rules, i, options){
    var date = Date.create(field.val());
    if (date.isAfter(Date.create('today')) ) {
        return "Date cannot be after today.";
    }
    else if (date.isBefore(Date.create('1970'))) {
        return "Date cannot be before 1970.";
    }
    else if (!field.val().match(/^\d{2}\/\d{2}\/\d{4}$/)) {
        return "Date must be in format MM/DD/YYYY";
    }
}

/*
    Site wide function to require length entry for captures,
    currently length is only required for
    Redfish, Speckled Trout, Red Snapper, and Yellowfin Tuna
    1, 2, 48, and 55 respectively
 */
function lengthRequired(field, rules, i, options){
    var species = field.closest('.capture-card').find('.speciesField').val();
    if(species == '1' || species == '48' || species == '55' || species == '2'){
        if(field.val() == ''){
            rules.push('required');
        }
    }
}




