(function($){
    $.fn.validationEngineLanguage = function(){
    };
    $.validationEngineLanguage = {
        newLang: function(){
            $.validationEngineLanguage.allRules = {
                // CUSTOM VALIDATOR
                "ajaxTagNoValidate": {
                  "url": "/tags/ajaxTagNumber",
                  // you may want to pass extra data on the ajax call
                  "alertText": "Tag does not exist",
                  "alertTextLoad": "Checking..."
                },
                // CUSTOM VALIDATOR
                "ajaxAnglerIdValidate": {
                  "url": "/anglers/ajaxAnglerId",
                  // you may want to pass extra data on the ajax call
                  "extraDataDynamic": ['#current_angler_id'],
                  "alertText": "Angler does not exist",
                  "alertTextLoad": "Checking..."
                },
                // CUSTOM VALIDATOR
                "ajaxEmailAlreadyExistsValidate": {
                    "url": "/sign_up/ajaxEmailAlreadyExists.json",
                    // you may want to pass extra data on the ajax call
                    "alertText": "Email is already taken",
                    "alertTextLoad": "Checking..."
                },
                "ajaxAnglerIdAlreadyExistsValidate": {
                  "url": "/anglers/ajaxAnglerIdAlreadyExists",
                  // you may want to pass extra data on the ajax call
                  "extraDataDynamic": ['#current_angler_id'],
                  "alertText": "Angler ID exists",
                  "alertTextLoad": "Checking..."
                },                                                                                                                        
                "onlyLetter": {
                    "regex": /^[a-zA-Z]+$/,
                    "alertText": "* Letters only"
                },
                "usDate": {
                  //	Check if date is valid by leap year
                  "func": function (field) {
                    var pattern = new RegExp(/^(0?[1-9]|1[012])[\/\-\.](0?[1-9]|[12][0-9]|3[01])[\/\-\.](\d{4})$/);
                    var match = pattern.exec(field.val());
                    if (match == null)
                      return false;

                    var year = match[3];
                    var month = match[1]*1;
                    var day = match[2]*1;
                    var date = new Date(year, month - 1, day); // because months starts from 0.

                    return (date.getFullYear() == year && date.getMonth() == (month - 1) && date.getDate() == day);
                  },
                  "alertText": "* Invalid date, must be in MM/DD/YYYY format"
                },
                "usPhone": {
                    "regex": /^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$/,
                    "alertText": "* Invalid Phone Number",
                    "alertText2": "Expected Format:",
                    "alertText3": "(999) 999-9999"
                },
                // CUSTOM VALIDATOR
                "zipCode": {
                    "regex": /^[0-9]{5}$/,
                    "alertText": "* Invalid Zip Code",
                    "alertText2": "Expected Format:",
                    "alertText3": "99999"
                }
            };
            
        }
    };

    $.validationEngineLanguage.newLang();
    
})(jQuery);
