class WelcomeController < ApplicationController
  # GET /welcome
  # GET /welcome.json
  def index
    if current_user
      redirect_to dashboard_path
      return
    end

    @news = get_news
    @stats = get_stats

    # get the blurb for the page
    @blurb = get_blurb 'contact_info'

    respond_to do |format|
      format.html { render :layout => 'welcome' }# index.html.erb
    end
  end

  private
    def get_news
      j = get_response_as_json("#{base_api_url}news.json", names_as_symbols: true)
      objects = j[:items]
      objects ||= []

      objects
    end

    def get_stats
      data = get_response_as_json("#{base_api_url}public_dashboard.json", names_as_symbols: true)[:items]

      {
          recaptures_by_species: [],
          captures_by_species: []
      }.deep_merge(data)

    end
end
