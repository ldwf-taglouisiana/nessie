class TagsController < ApplicationController

  before_filter :authenticate_user!


  # GET /tags/1
  # GET /tags/1.json
  def show

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /tags/new
  # GET /tags/new.json
  def new
    @blurb = get_blurb('order_tags')

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /tags
  # POST /tags.json
  def create

    #data = Hash.new
    #data['number_of_tags'] = params[:number]
    #data['tag_type'] = params[:type]
    #
    #to_send = Hash.new
    #to_send['tag_request'] = data
    data = Hash.new
    data[:items] = []
    data[:items] << { :tag_type => params[:type], :number_of_tags => params[:number]}

    parsed_json = access_token.post("#{base_api_url}tag_request.json",{:body => data.to_json, :headers => {'Content-Type' => 'application/json', 'Accept' => 'application/json'}}).parsed

    logger.info parsed_json

    if parsed_json['errors'].empty?
      flash.now[:notice] = 'Thank you for your tag request. Please allow for 1 week processing.'
    else
      flash.now[:error] = 'There was an error, please try again later'
    end

    respond_to do |format|
      format.html
      format.js
    end

  end

end
