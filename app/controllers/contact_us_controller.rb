class ContactUsController < ApplicationController

  def index
    @blurb = get_blurb('contact_us')

    respond_to do |format|
      format.html
    end
  end

  def site_feedback
   ContactUs.site_feedback(params).deliver
   redirect_to contact_us_path, notice: 'Thank you for submitting your feedback.'
  end
end