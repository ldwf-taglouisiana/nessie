class MyCapturesController < ApplicationController

  before_filter :authenticate_user!, :except => :get_captures

  # GET /my_captures
  # GET /my_captures.json
  def index
    @user_name = current_user.name

    @blurb = get_blurb('my_captures')

    @objects = access_token.get("#{base_api_url}captures.json").parsed

    @objects = sort(@objects)

    @objects = Kaminari.paginate_array(@objects).page(params[:page]).per(20)

    j = access_token.get("#{base_api_url}captures_dashboard.json").parsed


    @number_of_my_caps_this_year = j['my_captures_this_year']
    @number_of_my_recaps_this_year = j['my_recaptures_this_year']
    @number_of_my_recaps = j['my_recaptures']
    @all_my_caps = j['my_captures_ever']
    @caps_ever = j['captures_ever']
    @recaps_ever = j['recaptures_ever']

    j = access_token.get("#{base_api_url}combined_data.json").parsed

    @species = j['species']['items']
    @times = j['time_of_day_options']['items']

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /my_captures/1
  # GET /my_captures/1.json
  def show
    #@my_capture = MyCapture.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def review

    respond_to do |format|
      format.html
    end
  end


  def search

    data = Hash.new
    unless params[:start_date].nil? || params[:start_date].empty?
      data[:start_date] = Chronic.parse(params[:start_date]).utc
    end
    unless params[:end_date].nil? || params[:end_date].empty?
      data[:end_date] = Chronic.parse(params[:end_date]).utc
    end
    unless params[:species].nil? || params[:species] == 'Please Select...'
      data[:species] = params[:species]
    end

    @blurb = get_blurb('my_captures')

    @objects = access_token.get("#{base_api_url}captures.json", :params => data).parsed

    if sanitized_direction == "desc"
      @objects = @objects["items"].sort{|x,y| y[sanitized_sort] <=> x[sanitized_sort]}
    else
      @objects = @objects["items"].sort{|x,y| x[sanitized_sort] <=> y[sanitized_sort]}
    end

    @objects = Kaminari.paginate_array(@objects).page(params[:page]).per(20)

    j = access_token.get("#{base_api_url}combined_data.json").parsed

    @species = j['species']['items']
    @times = j['time_of_day_options']['items']

    respond_to do |format|
      format.js
      format.html { render :action => 'index'}
    end
  end

  # GET /my_captures/new
  # GET /my_captures/new.json
  def new
    j = access_token.get("#{base_api_url}new_draft_capture.json").parsed

    @event = j["draft_capture"]

    @blurb = get_blurb('new_capture')

    j = access_token.get("#{base_api_url}combined_data.json").parsed

    @species = j['species']['items']
    @species_lengths = j['species_lengths']['items']
    @times = j['time_of_day_options']['items']
    @conditions = j['fish_condition_options']['items']
    @dispositions = j['recapture_disposition_options']['items']

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
    j = access_token.get("#{base_api_url}draft_capture.json?id=#{params[:id]}").parsed

    @event = j["draft_capture"]

    @blurb = get_blurb('new_capture')

    j = access_token.get("#{base_api_url}combined_data.json").parsed

    @species = j['species']['items']
    @species_lengths = j['species_lengths']['items']
    @times = j['time_of_day_options']['items']
    @conditions = j['fish_condition_options']['items']
    @dispositions = j['recapture_disposition_options']['items']

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /my_captures
  # POST /my_captures.json
  def create
    captures = []
    save_as_draft = ''

    # hack to fix an issue where the save_as_draft param is misparsed by rails.
    params[:captures].each do |key, capture|
      if capture[:save_as_draft] != save_as_draft and capture[:save_as_draft] != ''
        save_as_draft = capture[:save_as_draft]
      end
    end

    params[:captures].each do |key, capture|
      capture_date = Chronic.parse(capture[:date]).utc
      comments = capture[:comments]
      entered_gps_type = capture[:entered_gps_type]
      fish_condition_id = capture[:fish_condition_id]
      lat = capture[:latitude]
      long = capture[:longitude]
      recapture = capture[:recapture]
      recapture_disposition_id = capture[:recapture_disposition_id]
      species_id = capture[:species_id]
      tag_number = capture[:tag_number].upcase.gsub(/\s+/,'')
      time_of_day = capture[:time_of_day_id]
      length = capture[:length]
      species_length_id = capture[:species_length_id]
      should_save = (save_as_draft == 'false') # false == save for later, true == submit
      location_description = capture[:location_description]
      uuid = capture[:uuid]

      captures << { :capture_date => capture_date, :comments => comments, :entered_gps_type => entered_gps_type, :fish_condition_id => fish_condition_id,
                    :latitude => lat, :longitude => long, :recapture => recapture, :recapture_disposition_id => recapture_disposition_id,
                    :species_id => species_id, :tag_number => tag_number, :time_of_day_id => time_of_day, :species_length_id => species_length_id,
                    :length => length, :should_save => should_save, :location_description => location_description, :uuid => uuid }
    end

    data = Hash.new
    data[:items] = captures

    j = access_token.post("#{base_api_url}draft_captures.json", { :body => data.to_json, :headers =>  {'Content-Type' => 'application/json', 'Accept' => 'application/json' }}).parsed

    respond_to do |format|
      if j['errors'].empty?
        format.js { render :json => "window.location.replace('#{ thank_you_path }');"}
      else
        flash[:error] = j['errors'].join('<br/>')
        format.js
      end
    end
  end

  def update
    captures = []

    id = params[:id]
    params[:captures].each do |key, capture|
      capture_date = Chronic.parse(capture[:date]).utc
      comments = capture[:comments]
      entered_gps_type = capture[:entered_gps_type]
      fish_condition_id = capture[:fish_condition_id]
      lat = capture[:latitude]
      long = capture[:longitude]
      recapture = capture[:recapture]
      recapture_disposition_id = capture[:recapture_disposition_id]
      species_id = capture[:species_id]
      tag_number = capture[:tag_number].upcase.gsub(/\s+/,'')
      time_of_day = capture[:time_of_day_id]
      length = capture[:length]
      species_length_id = capture[:species_length_id]
      should_save = (capture[:save_as_draft] == 'false')
      location_description = capture[:location_description]

    captures << { :id => id, :capture_date => capture_date, :comments => comments, :entered_gps_type => entered_gps_type, :fish_condition_id => fish_condition_id,
                  :latitude => lat, :longitude => long, :recapture => recapture, :recapture_disposition_id => recapture_disposition_id,
                  :species_id => species_id, :tag_number => tag_number, :time_of_day_id => time_of_day, :length => length, :species_length_id => species_length_id,
                  :should_save => should_save, :location_description => location_description}
    end

    data = Hash.new
    data[:items] = captures
    j = access_token.post("#{base_api_url}update_draft_capture.json", { :params => data, :headers =>  {'Content-Type' => 'application/json', 'Accept' => 'application/json' }}).parsed
    respond_to do |format|
      if j['errors'].empty?
        format.js { render :json => "window.location.replace('#{ draft_captures_path }');"}
      else
        flash[:error] = j['errors'].join('<br/>')
        format.js
      end
    end
  end

  def check_dup_caps
    j = access_token.get("#{base_api_url}check_for_duplicate_captures.json?tag_number=#{params[:tag_number]}").parsed

    render :json => j

  end

  def draft_capture_index
    @user_name = current_user.name

    @blurb = get_blurb('draft_captures')

    @objects = access_token.get("#{base_api_url}draft_captures.json").parsed

    if sanitized_direction == "desc"
      @objects = @objects["items"].sort{|x,y| y[sanitized_sort] <=> x[sanitized_sort]}
    else
      @objects = @objects["items"].sort{|x,y| x[sanitized_sort] <=> y[sanitized_sort]}
    end
    @objects = Kaminari.paginate_array(@objects).page(params[:page]).per(20)

    @species = access_token.get("#{base_api_url}species.json").parsed
    @species = @species["items"]

    respond_to do |format|
      format.html
    end
  end

  def draft_capture_search

    data = Hash.new
    unless params[:start_date].nil? || params[:start_date].empty?
      data[:start_date] = Chronic.parse(params[:start_date]).try(:utc)
    end
    unless params[:end_date].nil? || params[:end_date].empty?
      data[:end_date] = Chronic.parse(params[:end_date]).try(:utc)
    end
    unless params[:species].nil? || params[:species] == 'Please Select...'
      data[:species] = params[:species]
    end

    @blurb = get_blurb('my_captures')

    @objects = access_token.get("#{base_api_url}draft_captures.json", :params => data).parsed

    if sanitized_direction == "desc"
      @objects = @objects["items"].sort{|x,y| y[sanitized_sort] <=> x[sanitized_sort]}
    else
      @objects = @objects["items"].sort{|x,y| x[sanitized_sort] <=> y[sanitized_sort]}
    end

    @objects = Kaminari.paginate_array(@objects).page(params[:page]).per(20)

    j = access_token.get("#{base_api_url}combined_data.json").parsed

    @species = j['species']['items']
    @times = j['time_of_day_options']['items']

    respond_to do |format|
      format.js
      format.html { render :action => 'draft_capture_index'}
    end
  end

  def change_draft_capture_should_save
    j = access_token.post("#{base_api_url}change_draft_capture_should_save.json?id=#{params[:id]}").parsed

    render :json => j
  end

  def get_captures
    j = access_token.get("#{base_api_url}tag_history.json?tag_number=#{params[:tag_number]}").parsed

    render :json => j
  end

  def get_capture_report
    j = access_token.post("#{base_api_url}get_angler_capture_report.json").parsed

    respond_to do |format|
      format.json {
        render :json => j
      }
      format.xlsx {

        data = j.deep_symbolize_keys


        p = Axlsx::Package.new
        wb = p.workbook
        wb.add_worksheet(:name => 'Captures') do |sheet|
          sheet.add_row ['Tag Number', 'Species', 'Date', 'Latitude', 'Longitude', 'Location Description', 'Has Recapture']
          data[:captures].each do |capture|
            sheet.add_row [capture[:tag_number], capture[:species], capture[:capture_date], capture[:latitude],
                           capture[:longitude], capture[:location_description], capture[:has_recapture]]
          end
        end

        send_data p.to_stream.read, :filename => 'Captures_Spreadsheet.xlsx'
      }
    end
  end

  private

    def sort(array)

      boolean_fields = ['has_recapture']

      if sanitized_direction == "desc"

        if boolean_fields.include? sanitized_sort
          array = array["items"].sort{|x| (x[sanitized_sort] ? 1 : -1)}
        else
          array = array["items"].sort{|x,y| y[sanitized_sort] <=> x[sanitized_sort]}
        end

      else

        if boolean_fields.include? sanitized_sort
          array = array["items"].sort{|x| (x[sanitized_sort] ? -1 : 1)}
        else
          array = array["items"].sort{|x,y| x[sanitized_sort] <=> y[sanitized_sort]}
        end

      end

      array
    end

    def sanitized_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "desc"
    end

    def sanitized_sort
      unless @objects["items"].first.nil?
        @objects["items"].first.keys.include?(params[:sort]) ? params[:sort] : "tag_number"
      end
    end
end
