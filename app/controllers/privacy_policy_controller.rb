class PrivacyPolicyController < ApplicationController

  def index
    @blurb = get_blurb('about_program')
    @privacy_policy = get_blurb('privacy_policy')

    respond_to do |format|
      format.html # index.html.erb
    end
  end


end