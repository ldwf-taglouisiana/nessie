class VolunteerHoursController < ApplicationController

  before_filter :authenticate_user!

  # GET /volunteer_hours
  # GET /volunteer_hours.json
  def index
    redirect_to :action => 'new'
  end

  # GET /volunteer_hours/new
  # GET /volunteer_hours/new.json
  def new
    @blurb = get_blurb('volunteer_hours')

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /volunteer_hours
  # POST /volunteer_hours.json
  def create
    start_date = Chronic.parse(params[:start_date] + ' ' + params[:real_start_time]).utc
    end_date = Chronic.parse(params[:start_date] + ' ' + params[:real_end_time]).utc
    # If the start day is after the end day, bump the end date forward by one day
    if start_date > end_date
      end_date = end_date + 1.day
    end
    verified = params[:verified] ? true : false

    data = Hash.new
    data[:items] = []
    data[:items] << { :start_date => start_date, :end_date => end_date, :verified => verified, uuid: SecureRandom.uuid }

    begin
      parsed_json = access_token.post("#{base_api_url}volunteer_hours.json",{:body => data.to_json, :headers => {'Content-Type' => 'application/json', 'Accept' => 'application/json'}}).parsed
    rescue  OAuth2::Error => exception
      #this type of error can occur when a time chunk overlaps with another previously entered time
      unless exception.response.status == 500
        parsed_json = {}
        parsed_json['errors'] = exception.message
      else   #excetpion is 500
        super.handle_exception(exception)
      end


    end

    unless parsed_json['errors'].empty?
      flash[:error] = parsed_json['errors']
    end

    respond_to do |format|
      if parsed_json['errors'].empty?

        if params[:create_and_enter_capture]
          format.js { render :js => "window.location.replace('#{ new_my_capture_path }');"}
        else
          flash.now[:notice] = 'Hours have been added'
          format.js
        end

      end

      format.html
      format.js

    end
  end

end
