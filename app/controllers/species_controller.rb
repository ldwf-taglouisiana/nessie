class SpeciesController < ApplicationController
  # GET /species
  # GET /species.json
  def index
    url = "#{base_api_url}species.json?image_size=index_list#{ params[:target] ? '&target=true' : ''}"
    @blurb = params[:target] ? get_blurb('target_species') : get_blurb('common_fish')
    @objects = get_response_as_json(url)["items"]

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /species/1
  # GET /species/1.json
  def show
    url = "#{base_api_url}species.json?image_size=feature&id=#{params[:id]}"
    @blurb = get_blurb('common_fish')
    @species = get_response_as_json(url)['items'].first

    logger.info @species

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def search
    url = "#{base_api_url}species.json?image_size=index_list&term=#{input_text}"
    @blurb = params[:target] ? get_blurb('target_species') : get_blurb('common_fish')
    @objects = get_response_as_json(url)["items"]

    respond_to do |format|
      format.js # index.html.erb
    end
  end

  def clear_search
    url = "#{base_api_url}species.json?image_size=index_list#{ params[:target] ? '&target=true' : ''}"
    @blurb = params[:target] ? get_blurb('target_species') : get_blurb('common_fish')
    @objects = get_response_as_json(url)["items"]

    respond_to do |format|
      format.js # index.html.erb
    end
  end

  def image
    species = Species.find(params[:id])
    style = params[:style] ? params[:style] : 'original'
    send_file(species.photo.path(style), { :type => species.photo_content_type, :disposition => 'inline' } )
  end

end
