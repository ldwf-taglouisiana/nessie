class MarinasController < ApplicationController
  # GET /marinas
  # GET /marinas.json
  def index
    url = "#{base_api_url}marinas.json?image_size=index_list&parish=true"
    @blurb = get_blurb('find_a_spot')
    j = get_response_as_json(url)

    count = 0
    @parishes = j["items"]
    @parishes.each do |z|
      count = count + z.last.length
    end

    @parishes_1 = []
    @parishes_2 = []
    t_count = 0

    @parishes.each do |p|
      @parishes_1 << p if t_count < count/2
      @parishes_2 << p if t_count > count/2
      t_count = t_count + p.last.length
    end

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def search
    url = "#{base_api_url}marinas.json?image_size=index_list&parish=true&zip_code=#{params[:zipcode]}&dis=#{params[:distance]}"
    j = get_response_as_json(url)
    count = 0
    @parishes = j["items"]
    @parishes.each do |z|
      count = count + z.last.length
    end

    @parishes_1 = []
    @parishes_2 = []
    t_count = 0

    @parishes.each do |p|
      @parishes_1 << p if t_count < count/2
      @parishes_2 << p if t_count > count/2
      t_count = t_count + p.last.length
    end

    respond_to do |format|
      format.js # index.html.erb
    end
  end

  def clear_search
    url = "#{base_api_url}marinas.json?image_size=index_list&parish=true"
    @blurb = get_blurb('find_a_spot')
    j = get_response_as_json(url)

    count = 0
    @parishes = j["items"]
    @parishes.each do |z|
      count = count + z.last.length
    end

    @parishes_1 = []
    @parishes_2 = []
    t_count = 0

    @parishes.each do |p|
      @parishes_1 << p if t_count < count/2
      @parishes_2 << p if t_count > count/2
      t_count = t_count + p.last.length
    end

    respond_to do |format|
      format.js # index.html.erb
    end
  end

end
