class UserController < ApplicationController
  def edit
    @blurb = get_blurb('account_info')

    j =  access_token.get("#{base_api_url}/me.json")
    @user_info = j.parsed['user'].symbolize_keys
    @angler_info = j.parsed['angler'].symbolize_keys
    respond_to do |format|
      format.html
    end
  end

  def update
    result = nil
    j = nil
    begin
      # TODO: find a way to get the error out of the access_token, instead of assuming the problem was an incorrect password
      result = access_token.post("#{base_api_url}/update_me.json",{:params => params})
      j = JSON.parse(result.body,:symbolize_names => true)
    rescue => error
      j = Hash.new
      # OAuth errors start with a ':' for some reason, so trim it
      error.message.slice!(0)
      j[:message] = JSON.parse(error.message)["message"]
    end

    respond_to do |format|
      if !result.nil? and result.status == 200
        flash[:success] = j[:message]
        format.html { redirect_to users_edit_path}
      else
        flash[:error] = j[:message]
        format.html { redirect_to users_edit_path}
      end
    end
  end
end
