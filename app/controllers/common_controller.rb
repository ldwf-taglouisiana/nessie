class CommonController < ApplicationController

  def thank_you
    @blurb = get_blurb('thank_you')
    @signed_in = signed_in?
    respond_to do |format|
      format.html
    end
  end
end
