class FaqController < ApplicationController

  def index
    @blurb = get_blurb('about_program')
    @faq = get_blurb('faq')

    respond_to do |format|
      format.html # index.html.erb
    end
  end


end