class ApplicationController < ActionController::Base
  include RemoteDataHelper

  protect_from_forgery
  before_filter :access_token
  before_filter :get_announcements


  # default crud operations
  def index
    not_found
  end

  def show
    not_found
  end

  def new
    not_found
  end

  def create
    not_found
  end

  def edit
    not_found
  end

  def update
    not_found
  end

  #####################################


  rescue_from OAuth2::Error do |exception|
    handle_exception(exception)
  end

  private

  #this makes it a little easier to handle specific errors inside inheriting controllers
  def handle_exception(exception)
    if exception.response.status == 401
      current_user.try(:sign_out) unless current_user.nil?
      session[:user_id] = nil
      redirect_to root_url, alert: "Access token expired, try signing in again."
    end

  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def oauth_client
    @oauth_client ||= OAuth2::Client.new(ENV['APP_ID'], ENV['APP_SECRET'], site: API_SERVER['base_url'])
  end

  def access_token
    if current_user and current_user.access_token
      @access_token ||= OAuth2::AccessToken.new(oauth_client, current_user.access_token)
    end
  end

  def get_announcements
    @announcement = get_response_as_json("#{base_api_url}announcements.json")['items']

    @announcement = @announcement.empty? ? nil : @announcement.first['content']
  end

  helper_method :current_user

end

