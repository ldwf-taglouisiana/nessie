
class SessionsController < Devise::SessionsController
  include SignInModule, RemoteDataHelper

  #Got this so that we can sign out without throwing any flash messages around
  # DELETE /resource/sign_out
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    yield if block_given?
    respond_to_on_destroy
  end

  def new
    @blurb = get_blurb('login')
    respond_to do |format|
      format.html
    end
  end

  def create
    module_sign_in({:email => params[:email], :password => params[:password]})
  end


  def help
    respond_to do |format|
      format.html
    end
  end

  def request_reset

    if verify_recaptcha
      user = User.where('email ilike ?',params[:email]).first

      if !user.nil?
        token = ''

        3.times do
          token += Devise.friendly_token
        end

        user.reset_password_token = token
        user.save!

        PasswordResetRequest.password_reset_request(user,token).deliver

        flash[:notice] = "You should receive an email with password reset instructions shortly."

      else
        flash[:error] = %Q[Could not reset password. Please contact us <a href="#{contact_us_path}" style="color:#0066FF">here</a>.]
      end


    else
      flash[:error] = 'The reCaptcha was invalid'
    end

    respond_to do |format|
      format.js
    end

  end

  def reset_password
    @token = params[:reset_password_token]
    respond_to do |format|
      format.html
      format.js
    end


  end

  def send_reset_request
    if params[:reset_password_token] != nil
      user = User.where('reset_password_token = ?',params[:reset_password_token]).first
      if user
        url = "#{base_api_url}reset_password"
        j = post_data_to_url_json url, {
            body: {
                token: APPLICATION_HASH,
                email: user.email,
                password: params[:password],
                password_confirmation: params[:password_confirmation]
            }.to_json,
            headers: {
                'Content-Type' => 'application/json'
            }
        }

        if j['change_confirmation']
          flash.now[:notice] = "Password changed."
          user.reset_password_token = nil
          user.save!

        else
          flash[:error] = "Update failed, please try again."

        end
      else
        flash[:error] = "Token invalid"
      end
    end
    respond_to do |format|
      format.js
    end

  end

  private
  #required for destroy, from devi
  def respond_to_on_destroy
    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to after_sign_out_path_for(resource_name) }
    end
  end

end