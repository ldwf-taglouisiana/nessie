class NonRegisteredCapturesController < ApplicationController

  def new
    @blurb = get_blurb('non_registered_capture')

    j = get_response_as_json("#{base_api_url}combined_data.json")

    @species = j['species']['items']
    @species_lengths = j['species_lengths']['items']
    @times = j['time_of_day_options']['items']
    @conditions = j['fish_condition_options']['items']
    @dispositions = j['recapture_disposition_options']['items']

    j = get_response_as_json("#{base_api_url}new_draft_capture.json")

    @event = j["draft_capture"]

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # POST /my_captures
  # POST /my_captures.json
  def create

    if verify_recaptcha
      capture_params = params[:captures]
      data = Hash.new
      data[:items] = []

      capture = Hash.new
      #reformat the params to meet the expectations of the api

      capture[:capture_date] = Chronic.parse(capture_params[:date]).utc
      capture[:time_of_day_id] = capture_params[:time_of_day_id]
      capture[:tag_number] = capture_params[:tag_number]
      capture[:location_description] = capture_params[:location_description]
      capture[:latitude] = capture_params[:latitude]
      capture[:longitude] = capture_params[:longitude]
      capture[:species_id] = capture_params[:species_id]
      capture[:species_length_id] = capture_params[:species_length_id]
      capture[:length] = capture_params[:length]
      capture[:fish_condition_id] = capture_params[:fish_condition_id]
      capture[:recapture_disposition_id] = capture_params[:recapture_disposition_id]
      capture[:comments] = capture_params[:comments]
      capture[:entered_gps_type] = capture_params[:entered_gps_type]


      angler = params[:angler]

      data[:items] << { :capture => capture, :angler => angler }

      url = "#{base_api_url}add_unregistered_capture.json?token=#{APPLICATION_HASH}"
      parsed_json = post_data_to_url_json url, {
          body: data.to_json,
          headers: {
              'Content-Type' => 'application/json'
          }
      }
    else
      # reCaptcha failed
      parsed_json = {}
      parsed_json['errors'] = ['reCaptcha Invalid']
    end

    respond_to do |format|

      puts parsed_json['errors']
      if parsed_json['errors'].empty?
        #TODO this crashes the backend server for some reason
        format.js { render :js => "window.location.replace('#{ thank_you_path }');"}
      else
        flash[:error] = parsed_json['errors'].join('<br/>')
        format.js
      end
    end
  end

end
