class AboutController < ApplicationController
  # GET /about
  # GET /about.json
  def index
    url = "#{base_api_url}about_tagging_program.json"
    @blurb = get_blurb('about_program')
    @objects = get_response_as_json(url)["items"]

    respond_to do |format|
      format.html # index.html.erb
    end
  end

end