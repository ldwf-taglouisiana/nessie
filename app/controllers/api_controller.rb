class ApiController < ApplicationController
     def capture_map_data
       url = "#{base_api_url}capture_map_data.json?#{params.to_query}"
       render :json => get_response(url).body
       end

     def get_photos
       url = "#{base_api_url}photo_stream.json?#{params.to_query}"
       render :json => get_response(url).body
     end

     def check_duplicate_capture
       json = [params[:fieldId], true]

       # if it is not marked as a recapture then check with the api server
       if params[:recaptureValue] == 'false'
         url = "#{base_api_url}check_for_duplicate_captures.json?tag_number=#{params[:fieldValue]}"
         data = get_response_as_json(url, names_as_symbols: true)
         json = [ params[:fieldId], data[:result] ]
       end

       render :json => json
     end

     def check_tag_exists

       data = {}

       # if signed in then use an authenticated route
       if current_user
         url = "#{base_api_url}check_tag_exists_for_angler.json?fieldValue=#{params[:fieldValue]}&fieldId=#{params[:fieldId]}&secret_recap=#{params[:recaptureValue]}"
         data = access_token.get(url).parsed

       # use the public route, when not signed in
       else
         url = "#{base_api_url}public_check_tag_exists.json?fieldValue=#{params[:fieldValue]}&fieldId=#{params[:fieldId]}&secret_recap=#{params[:recaptureValue]}"
         data = get_response_as_json(url, names_as_symbols: true)
       end

       render :json => data
     end
end