class DashboardsController < ApplicationController
  before_filter :authenticate_user!

  # GET /dashboards
  # GET /dashboards.json
  def index
    @user_name = current_user.name

    @blurb = get_blurb('capture_dashboard')

    j = access_token.get("#{base_api_url}captures_dashboard.json").parsed.symbolize_keys
    j = JSON.parse( j.to_json, {:symbolize_names => true} )[:items]

    @number_of_my_caps_this_year = j[:my_total_captures_this_year][:count].nil? ? 0 : j[:my_total_captures_this_year][:count]
    @number_of_my_recaps_this_year = j[:my_total_recaptures_this_year][:count]
    @number_of_my_caps_last_season = j[:my_total_captures_last_season][:count].nil? ? 0 : j[:my_total_captures_last_season][:count]
    @number_of_my_recaps_last_season = j[:my_total_recaptures_last_season][:count]
    @number_of_my_recaps = j[:my_total_recaptures][:count]
    @all_my_caps = j[:my_total_captures][:count]
    @caps_ever = j[:total_captures][:count]
    @recaps_ever = j[:total_recaptures][:count]

    @captures_by_species = j[:captures_by_species].nil? ? [] : j[:captures_by_species]
    @recaptures_by_species = j[:recaptures_by_species].nil? ? [] : j[:recaptures_by_species]

    @angler_captures_by_species = j[:angler_captures_by_species].nil? ? [] : j[:angler_captures_by_species]
    @angler_recaptures_by_species = j[:angler_recaptures_by_species].nil? ? [] : j[:angler_recaptures_by_species]

    @captures_by_species_total = 0
    @recaptures_by_species_total = 0
    @angler_captures_by_species_total = 0
    @angler_recaptures_by_species_total = 0

    @captures_by_species.each do |species|
      @captures_by_species_total += species[:species_count]
    end

    @recaptures_by_species.each do |species|
      @recaptures_by_species_total += species[:species_count]
    end

    @angler_captures_by_species.each do |species|
      @angler_captures_by_species_total += species[:species_count]
    end

    @angler_recaptures_by_species.each do |species|
      @angler_recaptures_by_species_total += species[:species_count]
    end

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /dashboards/1
  # GET /dashboards/1.json
  def show
    @dashboard = Dashboard.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dashboard }
    end
  end

  # GET /dashboards/new
  # GET /dashboards/new.json
  def new
    @dashboard = Dashboard.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dashboard }
    end
  end

  # GET /dashboards/1/edit
  def edit
    @dashboard = Dashboard.find(params[:id])
  end

  # POST /dashboards
  # POST /dashboards.json
  def create
    @dashboard = Dashboard.new(params[:dashboard])

    respond_to do |format|
      if @dashboard.save
        format.html { redirect_to @dashboard, notice: 'Dashboard was successfully created.' }
        format.json { render json: @dashboard, status: :created, location: @dashboard }
      else
        format.html { render action: "new" }
        format.json { render json: @dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dashboards/1
  # PUT /dashboards/1.json
  def update
    @dashboard = Dashboard.find(params[:id])

    respond_to do |format|
      if @dashboard.update_attributes(params[:dashboard])
        format.html { redirect_to @dashboard, notice: 'Dashboard was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dashboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dashboards/1
  # DELETE /dashboards/1.json
  def destroy
    @dashboard = Dashboard.find(params[:id])
    @dashboard.destroy

    respond_to do |format|
      format.html { redirect_to dashboards_url }
      format.json { head :no_content }
    end
  end
end
