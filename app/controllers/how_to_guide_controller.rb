class HowToGuideController < ApplicationController
  # GET /how_to_guide
  # GET /how_to_guide.json
  def index
    url = "#{base_api_url}how_to_tag.json"

    @blurb = get_blurb('how_to')
    @objects = get_response_as_json(url)["items"]

    respond_to do |format|
      format.html # index.html.erb
    end
  end

end
