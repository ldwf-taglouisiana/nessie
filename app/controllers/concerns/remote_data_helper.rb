module RemoteDataHelper
  def base_url
    API_SERVER["base_url"]
  end

  def base_api_url
    API_SERVER['address']
  end

  def get_blurb(blurb)
    j = get_response_as_json("#{base_api_url}blurbs.json?includes=#{blurb}")

    @objects = j['items'].first["#{blurb}"]

    @objects = @objects.nil? ? '' : @objects

    @objects
  end

  def get_response_as_json(url, options = {})
    JSON.parse get_response(url, options).body, symbolize_names: options[:names_as_symbols].present?
  end

  def get_response(url, options = {})
    options = options.merge({ method: :get })
    make_request_from_url(url, options)
  end

  def post_data_to_url_json(url, options = {})
    JSON.parse post_data_to_url(url, options).body, symbolize_names: options[:names_as_symbols].present?
  end

  def post_data_to_url(url, options = {})
    options = options.merge({ method: :post })
    make_request_from_url(url, options)
  end

  def make_request_from_url(url, options = {})
    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = base_api_url.match(/^http:\/\/(localhost|192.168)/).nil?

    case options[:method]
      when :post
        request =  Net::HTTP::Post.new(uri.request_uri)
      else
        request =  Net::HTTP::Get.new(uri.request_uri)
    end

    request.body = options[:body] if options[:body].present?

    options[:headers].try(:each)do |k, v|
      request.add_field(k,v)
    end

    http.request(request)
  end

  def get_contact_blurb
    get_blurb 'contact_info'
  end
end