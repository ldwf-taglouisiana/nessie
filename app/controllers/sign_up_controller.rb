require 'sign_in_module'
class SignUpController < ApplicationController
  include SignInModule

  # GET /sign_up/new
  # GET /sign_up/new.json
  def new

    if current_user
      redirect_to dashboard_path
      return
    end

    #@sign_up = SignUp.new
    @blurb = get_blurb('sign_up')

    #################################

    url = "#{base_api_url}shirt_size_options.json"
    @shirt_sizes = get_response_as_json(url)['items']

    ###################################

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sign_up }
    end
  end

  # POST /sign_up
  # POST /sign_up.json
  def create

    if params[:override] == '6nyVFTcqBtsSVCFYwEQXvTRSVkbrTaGvVJORUGj7' || verify_recaptcha
      angler = Hash.new
      angler[:first_name]     = params[:first_name]
      angler[:last_name]      = params[:last_name]
      angler[:street]         = params[:street]
      angler[:suite]          = params[:suite]
      angler[:city]           = params[:city]
      angler[:state]          = params[:state]
      angler[:zip]            = params[:zip]
      angler[:phone_number_1] = params[:phone_number_1]
      angler[:phone_number_2] = params[:phone_number_2]
      angler[:email]          = params[:email]
      angler[:shirt_size]     = params[:shirt_size]
      angler[:comments]       = params[:comments]

      user = Hash.new
      user[:first_name] = params[:first_name]
      user[:last_name]  = params[:last_name]
      user[:email]      = params[:email]
      user[:password]   = params[:password]
      user[:password_confirmation] = params[:password_confirmation]

      data = Hash.new
      data[:angler] = angler
      data[:user] = user
      #add the application hash so we are authorized to make this angler

      url = "#{base_api_url}register.json?token=#{APPLICATION_HASH}"
      parsed_json = post_data_to_url_json url, {
          body: data.to_json,
          headers: {
              'Content-Type' => 'application/json'
          }
      }
    else
      parsed_json = {}
      parsed_json['errors'] = ['Invalid reCaptcha response']
    end

    respond_to do |format|
      format.js {
        if parsed_json['errors'].empty?
          module_sign_in({ :email => params[:email], :password => params[:password]})
        else
          flash.now[:error] = parsed_json['errors'][0]
        end
      }

    end

  end


  def check_if_email_exists
    url = "#{base_api_url}/ajaxEmailAlreadyExists.json?fieldId=#{params[:fieldId]}&fieldValue=#{params[:fieldValue]}"
    render :json => get_response_as_json(url)
  end

end
