# Controller needed for Devise's "registrations" views. Copy of sign_up_controller.

class RegistrationsController < Devise::RegistrationsController
  RemoteDataHelper

  # GET /sign_up/new
  # GET /sign_up/new.json
  def new
    raise ActionController::RoutingError.new('Not Found')
  end

  # POST /sign_up
  # POST /sign_up.json
  def create
    raise ActionController::RoutingError.new('Not Found')
  end

end
