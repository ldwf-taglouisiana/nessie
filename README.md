## Start the vagrant machine

If this is the first start up (provision) then we have options to get the machine setup faster.

We are using multi VM so that the nessie box can communicate with the Narwhal server.
This mimics a similar multi box setup. Which gives us a better dev environment.

As of Vagrant 1.8.x we can use the new linked_clone command which gives us differential disks,
reducing host hard disk space.

By default the Narwhal boots a new VM in the 'dev' branch using a fresh copy of the remote database.
We can skip some of these options with ENV variables.

Running the fallowing will change the branch Narwhal:

```
NARWHAL_BRANCH=master vagrant up
```

We can tell the VM to use the host's current Narwhal dev directory, instead of cloning from git (only effects provisioning):

```
NARWHAL_PATH=/path/to/dev/narwhal vagrant up
```

Setting the NARWHAL_PATH will enable NFS on that directory so any changed on the host will cascade to the
the VM here.

We can also skip the remote DB download with the following (only effects provisioning):

```
NARWHAL_SKIP_DB_DOWNLOAD=true vagrant up
```

You can combine any of the options that you wish.

By default both servers start automatically on vagrant up

```bash
vagrant up
```

This will start 2 VMs, one being Narwhal, the other being Nessie.

## Startup

The rails servers start up automatically on both machine start up.

There are a number of alias provided to control the servers easily.

```
rails_start
rails_stop
rails_restart
rails_log
thin_log
byebug_connect (only on nessie as of now)
```

``stdout`` is redirected to ``thin_log``, such as ``puts``.
The standard rails log can be tailed with the previous 400 lines with ``rails_log``

## Access web portals

- You can access Nessie with `http://localhost:3000`
- You can access Narwhal with `http://localhost:4000`

## Access ssh

- You can access Nessie with `vagrant ssh` or `vagrant ssh nessie`
- You can access Narwhal with `vagrant ssh narwhal`

## Alias commands



## A `pg_dump` error during the provision of the Narwhal box

This indicates that one of the indexes on the remote was being updated while you were trying to dump it.

To restart the provision use `vagrant provision narwhal`, this will rerun the tool. You may have to do a few times to get a clean database dump.