# Load the rails application
require File.expand_path('../application', __FILE__)
require 'net/http'

# Initialize the rails application
FishTaggerFrontFacing::Application.initialize!

#Here we store the hash from this application
sha256 = OpenSSL::Digest::SHA256.new
sha256 << ENV['APP_ID']
sha256 << ENV['APP_SECRET']

APPLICATION_HASH = sha256.to_s
