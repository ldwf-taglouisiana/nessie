#! /bin/bash

cat << EOF >> /home/vagrant/.bashrc
alias rails_start='cd /vagrant && thin start --port 3000 --daemonize --pid /tmp/pids/narwhal_nessie_thin.pid --log /tmp/narwhal_nessie_server.log --environment development && cd -'
alias rails_stop='if [ -f /tmp/pids/thin.pid ]; then kill -9 \`cat /tmp/pids/thin.pid\` 2>/dev/null; fi'
alias rails_restart='rails_stop && rails_start'
alias rails_log='tail -f -n 400 /vagrant/log/development.log'
alias thin_log='tail -f -n 400 /tmp/narwhal_nessie_server.log'
alias byebug_connect='cd /vagrant && byebug -R localhost:5123 && cd -'
EOF