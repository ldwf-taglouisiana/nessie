#! /bin/bash

printenv

echo "Provisioning from the $1"

echo "Adding the gitlab host key"
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts;

echo "PATH=$PATH:/usr/share/postgresql/9.4/contrib/postgis-2.1/" >> ~/.bash_profile;
export PATH=$PATH:/usr/share/postgresql/9.4/contrib/postgis-2.1/;

echo "Installing Gems and setting up the database"
cd ~/workspace && bundle install && rake db:rebuild_from_remote SKIP_DOWNLOAD=$2 && rake db:migrate;