#! /bin/bash

cd ~/workspace;

echo "Checking out '$1' branch"
git fetch --all;
git checkout $1;
git pull origin $1;

echo "Installing Gems"
bundle install && rake db:migrate;

echo "Starting server on host port 4000"
rails_restart
echo "You can access the Narwhal server at http://localhost:4000"