#! /bin/bash

cd /vagrant;

echo "Installing gems and setting up the database"
bundle install;
rake db:create && rake db:migrate;

sed -i 's/^CONSOLE_NAME.*$/CONSOLE_NAME="Trident-Nessie"/' /home/vagrant/.bashrc

cat << EOF >> /home/vagrant/.bashrc
alias rails_start='cd /vagrant && thin -p 3000 --environment development -P /tmp/pids/thin.pid -l /tmp/rails_development.log -d start && cd -'
alias rails_stop='if [ -f /tmp/pids/thin.pid ]; then kill -9 \`cat /tmp/pids/thin.pid\` 2>/dev/null; fi'
alias rails_restart='rails_stop && rails_start'
alias rails_log='tail -f -n 400 /vagrant/log/development.log'
alias thin_log='tail -f -n 400 /tmp/rails_development.log'
alias byebug_connect='cd /vagrant && byebug -R localhost:5123 && cd -'
EOF


if [ ! -f config/local_env.yml ]; then
  echo "APP_ID: 'change me'" >> config/local_env.yml
  echo "APP_SECRET: 'change me'" >> config/local_env.yml
  echo "Update the config/local_env.yml file with the proper values."
fi