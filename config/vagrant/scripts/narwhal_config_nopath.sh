#! /bin/bash

printenv

echo "Provisioning from the gitlab repo"
echo "Adding the gitlab host key"
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts;

git clone --depth=1 git@gitlab.com:ldwf-taglouisiana/narwhal.git ~/web && cd ~/web && git fetch --all && git checkout $1
rm -rf ~/workspace
ln -s $HOME/web $HOME/workspace

echo "PATH=$PATH:/usr/share/postgresql/9.4/contrib/postgis-2.1/" >> ~/.bash_profile;
export PATH=$PATH:/usr/share/postgresql/9.4/contrib/postgis-2.1/;

cd ~/workspace;
echo "Installing Gems and setting up the database"
bundle install && rake db:rebuild_from_remote SKIP_DOWNLOAD=$2 && rake db:migrate;