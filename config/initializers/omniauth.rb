# Initializer to add the taglouisiana middleware to our app
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :taglouisiana, ENV['APP_ID'], ENV['APP_SECRET']
end