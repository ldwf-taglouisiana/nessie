# Be sure to restart your server when you modify this file.

FishTaggerFrontFacing::Application.config.session_store :cookie_store, key: '_fish_tagger_front_facing_session'

# Use the database for sessions_controller.rb instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# FishTaggerFrontFacing::Application.config.session_store :active_record_store
