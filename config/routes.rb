FishTaggerFrontFacing::Application.routes.draw do
  get 'users/edit' => 'user#edit'
  post 'users/update' => 'user#update'


  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :registrations => "registrations", :sessions => "sessions" }
  devise_scope :user do
    get  '/users/help' => 'sessions#help'
    post '/users/request_reset' => 'sessions#request_reset'
    get  '/users/reset_password' => 'sessions#reset_password'
    post '/users/send_reset_request' => 'sessions#send_reset_request'
  end

  get '/sign_up/ajaxEmailAlreadyExists' => 'sign_up#check_if_email_exists'

  resources :welcome
  resources :my_captures
  resources :marinas, except: [:show, :edit, :index]
  resources :tags
  resources :volunteer_hours
  resources :species, except: [:show, :edit]
  resources :sign_up
  resources :about
  resources :how_to_guide
  resources :contact_us

  resources :non_registered_captures

  scope 'api' do
    get '/capture_map_data' => 'api#capture_map_data'
    get '/photos' => 'api#get_photos'
    get '/tag_history' => 'my_captures#get_captures'
    get '/check_duplicate_capture' => 'api#check_duplicate_capture'
    get '/check_tag_exists' => 'api#check_tag_exists'
  end

  post '/marinas/search' => 'marinas#search'
  post '/marinas/clear_search' => 'marinas#clear_search'
  post '/species/search' => 'species#search'
  post '/species/clear_search' => 'species#clear_search'

  get '/thank_you' => 'common#thank_you'
  get '/species/image/:id/:image_name' => 'species#image'
  get '/check_captures' => 'my_captures#check_dup_caps'
  get '/draft_captures' => 'my_captures#draft_capture_index'
  post '/draft_captures_search' => 'my_captures#draft_capture_search'
  get '/draft_captures_search' => 'my_captures#draft_capture_search'
  get '/dashboard' => 'dashboards#index'

  post '/my_captures/:id' => 'my_captures#update'
  post '/my_captures/change_draft_capture_should_save/:id' => 'my_captures#change_draft_capture_should_save'
  get '/my_captures_report' => 'my_captures#get_capture_report'
  post '/my_captures_search' => 'my_captures#search'
  get '/my_captures_search' => 'my_captures#search'

  post '/contact_us/site_feedback' => 'contact_us#site_feedback'

  get '/faq' => 'faq#index'
  get '/privacy_policy' => 'privacy_policy#index'

  root :to => 'welcome#index'

end
